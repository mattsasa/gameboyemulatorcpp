#include <iostream>
#include <fstream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>

#include <SDL2/SDL.h>

using namespace cv;
using namespace std;

struct ProcessorFlags {
  bool z;
  bool n;
  bool h;
  bool cy;
};

struct Buttons {
  bool a, b, start, select, up, down, left, right;
};

struct SpriteData {
  bool below_background, vertical_flip, horiz_flip;
  uint16_t tile_start, palette_register;
  uint8_t top_offset, left_offset;
};

class ProcessorState {

  public:

    uint8_t a, b, c, d, e, h, l;
    uint16_t pc, sp;
    uint64_t cycles;
    uint8_t scrollX, scrollY, windowX, windowY;

    //0xFFFF + 0x200000 + 0x8000    Main memory map + 128 rom banks + 4 ram banks
    uint8_t memory[65536 + 2097152 + 32768];
    ProcessorFlags flags;
    uint8_t directionInputBits;
    uint8_t buttonInputBits;

    bool interrupt_enable;
    bool halted;

    bool cartIsMBCType;
    uint8_t romBankMode;
    uint8_t ramBankMode;

    char game_name[100];

    ProcessorState() {
      a = 17; b = 0; c = 13; d = 0; e = 216; h = 1; l = 77; sp = 65534; pc = 0;
      scrollX = 0; scrollY = 0; windowX = 0; windowY = 0;
      directionInputBits = 15; buttonInputBits = 15;
      flags.z = false; flags.n = false; flags.h = false; flags.cy = false;
      interrupt_enable = false; halted = false; cartIsMBCType = false;
      romBankMode = 0; ramBankMode = 0;
    }

    void writeStateToFile() {
      String saveLocation = String(game_name) + ".savestate";
      FILE *pFile = fopen (saveLocation.c_str(), "wb");
      fwrite(this, sizeof(ProcessorState), 1, pFile);
      cout << "Saved Game State" << endl;
    }

    void importStateFromFile() {
      String saveLocation = String(game_name) + ".savestate";
      FILE *pFile = fopen(saveLocation.c_str(), "rb");
      if (pFile == NULL) { cout << "Read Save File Error" << endl; exit (1); }
      fread(this, sizeof(ProcessorState), 1, pFile);
      cout << "Loaded Game State" << endl;
    }

};

bool processOpcode(ProcessorState *state);

pair<int,int> seperateBytes(uint16_t word);

void initiateDMATransfer(uint16_t start_address);

void setFlags(int a, int b, int setZ, int setN, int setH, int setCY);

void restoreFlagsFromByte(uint8_t byte);

uint8_t convertFlagsToByte();

uint32_t extendAddr(uint16_t address);

void overwriteBootRom();

bool isRom(uint16_t address);