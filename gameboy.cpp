#include "gameboy.h"

uint8_t initCartRom[256];
const int bootRom[256] = {
    0x31, 0xFE, 0xFF, 0xAF, 0x21, 0xFF, 0x9F, 0x32, 0xCB, 0x7C, 0x20, 0xFB, 0x21, 0x26, 0xFF, 0x0E,
    0x11, 0x3E, 0x80, 0x32, 0xE2, 0x0C, 0x3E, 0xF3, 0xE2, 0x32, 0x3E, 0x77, 0x77, 0x3E, 0xFC, 0xE0,
    0x47, 0x11, 0x04, 0x01, 0x21, 0x10, 0x80, 0x1A, 0xCD, 0x95, 0x00, 0xCD, 0x96, 0x00, 0x13, 0x7B,
    0xFE, 0x34, 0x20, 0xF3, 0x11, 0xD8, 0x00, 0x06, 0x08, 0x1A, 0x13, 0x22, 0x23, 0x05, 0x20, 0xF9,
    0x3E, 0x19, 0xEA, 0x10, 0x99, 0x21, 0x2F, 0x99, 0x0E, 0x0C, 0x3D, 0x28, 0x08, 0x32, 0x0D, 0x20,
    0xF9, 0x2E, 0x0F, 0x18, 0xF3, 0x67, 0x3E, 0x64, 0x57, 0xE0, 0x42, 0x3E, 0x91, 0xE0, 0x40, 0x04,
    0x1E, 0x02, 0x0E, 0x0C, 0xF0, 0x44, 0xFE, 0x90, 0x20, 0xFA, 0x0D, 0x20, 0xF7, 0x1D, 0x20, 0xF2,
    0x0E, 0x13, 0x24, 0x7C, 0x1E, 0x83, 0xFE, 0x62, 0x28, 0x06, 0x1E, 0xC1, 0xFE, 0x64, 0x20, 0x06,
    0x7B, 0xE2, 0x0C, 0x3E, 0x87, 0xE2, 0xF0, 0x42, 0x90, 0xE0, 0x42, 0x15, 0x20, 0xD2, 0x05, 0x20,
    0x4F, 0x16, 0x20, 0x18, 0xCB, 0x4F, 0x06, 0x04, 0xC5, 0xCB, 0x11, 0x17, 0xC1, 0xCB, 0x11, 0x17,
    0x05, 0x20, 0xF5, 0x22, 0x23, 0x22, 0x23, 0xC9, 0xCE, 0xED, 0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B,
    0x03, 0x73, 0x00, 0x83, 0x00, 0x0C, 0x00, 0x0D, 0x00, 0x08, 0x11, 0x1F, 0x88, 0x89, 0x00, 0x0E,
    0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD, 0xD9, 0x99, 0xBB, 0xBB, 0x67, 0x63, 0x6E, 0x0E, 0xEC, 0xCC,
    0xDD, 0xDC, 0x99, 0x9F, 0xBB, 0xB9, 0x33, 0x3E, 0x3C, 0x42, 0xB9, 0xA5, 0xB9, 0xA5, 0x42, 0x3C,
    0x21, 0x04, 0x01, 0x11, 0xA8, 0x00, 0x1A, 0x13, 0xBE, 0x00, 0x00, 0x23, 0x7D, 0xFE, 0x34, 0x20,
    0xF5, 0x06, 0x19, 0x78, 0x86, 0x23, 0x05, 0x20, 0xFB, 0x86, 0x00, 0x00, 0x3E, 0x01, 0xE0, 0x50
};

Mat background, window, screen;

SDL_Window* sdl_window;
SDL_Renderer* renderer;
SDL_Texture* gb_screen_texture;

ProcessorState state = ProcessorState();
Buttons buttons;

bool stop = false;

uint32_t extendAddr(uint16_t address) {
  if (address >= 0x4000 && address <= 0x7FFF && state.cartIsMBCType && state.romBankMode > 0) {
    return (0x4000 * state.romBankMode) + address + 0x4000;
  }
  if (address >= 0xA000 && address <= 0xBFFF && state.cartIsMBCType && state.ramBankMode > 0) {
    return (0x2000 * state.ramBankMode) + address + 0x207FFF;
  }
  return address;
}


bool isRom(uint16_t address) {
  uint32_t ext = extendAddr(address);
  if (ext >= 0 && ext <= 0x7FFF) { return true; }
  if (ext >= 0x10000 && ext <= 0x20FFFF) { return true; }

  return false;
}


void initiateDMATransfer(uint16_t start_address) {
  for (int i = 0; i < 160; i++) {
    state.memory[0xFE00 + i] = state.memory[extendAddr(start_address + i)];
  }
}

void writePixelToScreenAt2x(int y, int x, int color_bits, int palette_register) {
  if (y < 0 || x < 0 || y >= 144 || x >= 160) return;

  uint8_t color_value;
  switch (color_bits) {
    case 0: color_value = state.memory[palette_register] & 3; break;
    case 1: color_value = (state.memory[palette_register] & 12) >> 2; break;
    case 2: color_value = (state.memory[palette_register] & 48) >> 4; break;
    case 3: color_value = (state.memory[palette_register] & 192) >> 6; break;
  }

  Vec3b final_color;
  switch (color_value) {
    case 0: final_color = Vec3b(255,255,255); break;
    case 1: final_color = Vec3b(192,192,192); break;
    case 2: final_color = Vec3b(96,96,96); break;
    case 3: final_color = Vec3b(0,0,0); break;
  }

  screen.at<Vec3b>(y*2,x*2) = final_color;
  screen.at<Vec3b>(y*2,x*2+1) = final_color;
  screen.at<Vec3b>(y*2+1,x*2) = final_color;
  screen.at<Vec3b>(y*2+1,x*2+1) = final_color;
}

void copyWindowToScreen() {
  for (uint8_t h = 0; h < 144; h++) {
    for (uint8_t w = 0; w < 160; w++) {
      int h2 = h - state.windowY; int w2 = w + state.windowX - 7;
      if (h2 > 255 || h2 < 0 || w2 > 255) continue;
      writePixelToScreenAt2x(h,w, window.at<uchar>(h2, w2), 0xFF47);
    }
  }
}

void copyBackgroundtoScreen() {
  for (uint8_t h = 0; h < 144; h++) {
    for (uint8_t w = 0; w < 160; w++) {
      uint8_t h2 = h + state.scrollY; uint8_t w2 = w + state.scrollX;
      if (h2 > 255 || w2 > 255) continue;
      writePixelToScreenAt2x(h,w, background.at<uchar>(h2, w2), 0xFF47);
    }
  }
}

void fillSprite(SpriteData sprite) {
  uint8_t vram_offset = 0;
  for (int h2 = 0; h2 < 8; h2++) {
    uint8_t line_h = state.memory[sprite.tile_start + vram_offset];
    uint8_t line_l = state.memory[sprite.tile_start + vram_offset + 1];
    vram_offset += 2;
    for (int pixel = 7; pixel >= 0; pixel--) {
      uint8_t scren_h = sprite.vertical_flip ? sprite.top_offset + (7 - h2) : sprite.top_offset + h2;
      uint8_t screen_w = sprite.horiz_flip ? sprite.left_offset + pixel : sprite.left_offset + (7 - pixel);
      bool bit_h = (line_h & (1 << pixel));
      bool bit_l = (line_l & (1 << pixel));
      uint8_t color_bits;
      if (bit_h && bit_l) color_bits = 3;
      if (!bit_h && bit_l) color_bits = 2;
      if (bit_h && !bit_l) color_bits = 1;
      if (!bit_h && !bit_l) continue; // Don't draw white sprite background over background

      //Sprites below background only draw over white
      if (sprite.below_background && screen.at<Vec3b>(scren_h*2, screen_w*2) != Vec3b(255,255,255)) continue;

      writePixelToScreenAt2x(scren_h, screen_w, color_bits, sprite.palette_register);
    }
  }
}

void fillSprites() {
  bool tall_sprite_mode = (state.memory[0xff40] & (1 << 2)) != 0;

  for (int i = 0xFE00; i < 0xFE9F; i += 4) {
    SpriteData sprite;
    uint8_t tile_id = state.memory[i+2];
    bool palette = (state.memory[i+3] & (1 << 4)) != 0;
    sprite.below_background = (state.memory[i+3] & (1 << 7)) != 0;
    sprite.vertical_flip = (state.memory[i+3] & (1 << 6)) != 0;
    sprite.horiz_flip = (state.memory[i+3] & (1 << 5)) != 0;
    sprite.palette_register = palette ? 0xFF49 : 0xFF48;
    sprite.tile_start = 0x8000 + (tile_id * 16);
    sprite.top_offset = state.memory[i] - 16, sprite.left_offset = state.memory[i+1] - 8;
    if (tall_sprite_mode && sprite.vertical_flip) sprite.top_offset += 8;
    fillSprite(sprite);

    if (tall_sprite_mode) {
      sprite.tile_start += 16;
      sprite.vertical_flip ? sprite.top_offset -=8 : sprite.top_offset += 8;
      fillSprite(sprite);
    }

  }
}



void fillBackground() {

  uint8_t lcdc_byte = state.memory[0xff40];
  uint16_t tileMapCounter = (lcdc_byte & (1 << 3)) == 0 ? 0x9800 : 0x9C00;
  uint16_t tileDataTableLoc = (lcdc_byte & (1 << 4)) != 0 ? 0x8000 : 0x9000;

  for (int h = 0; h < 32; h++) {
    for (int w = 0; w < 32; w++) {
      uint16_t tile_start;
      if ((lcdc_byte & (1 << 4)) != 0) {
        uint8_t tile_id = state.memory[tileMapCounter];
        tile_start = tileDataTableLoc + (tile_id * 16);
      } else {
        int8_t tile_id = (int8_t)state.memory[tileMapCounter];
        tile_start = tileDataTableLoc + (tile_id * 16);
      }
      uint8_t vram_offset = 0, top = h * 8, left = w * 8;

      for (int h2 = 0; h2 < 8; h2++) {
        uint8_t line_h = state.memory[tile_start + vram_offset];
        uint8_t line_l = state.memory[tile_start + vram_offset + 1];
        vram_offset += 2;
        for (int pixel = 7; pixel >= 0; pixel--) {
          uint8_t w2 = 7 - pixel;
          bool bit_h = (line_h & (1 << pixel));
          bool bit_l = (line_l & (1 << pixel));
          uint8_t color_bits;
          if (bit_h && bit_l) color_bits = 3;
          if (!bit_h && bit_l) color_bits = 2;
          if (bit_h && !bit_l) color_bits = 1;
          if (!bit_h && !bit_l) color_bits = 0;
          background.at<uchar>(top + h2, left + w2) = color_bits;
        }
      }
      tileMapCounter++;
    }
  }

}

void fillWindow() {
  uint8_t lcdc_byte = state.memory[0xff40];
  uint16_t tileMapCounter = (lcdc_byte & (1 << 6)) == 0 ? 0x9800 : 0x9C00;
  uint16_t tileDataTableLoc = (lcdc_byte & (1 << 4)) != 0 ? 0x8000 : 0x9000;
  for (int h = 0; h < 32; h++) {
    for (int w = 0; w < 32; w++) {
      uint16_t tile_start;
      if ((lcdc_byte & (1 << 4)) != 0) {
        uint8_t tile_id = state.memory[tileMapCounter];
        tile_start = tile_id * 16;
      } else {
        int8_t tile_id = state.memory[tileMapCounter];
        tile_start = tile_id * 16;
      }
      tile_start += tileDataTableLoc;

      uint8_t vram_offset = 0, top = h * 8, left = w * 8;
      for (int h2 = 0; h2 < 8; h2++) {
        uint8_t line_h = state.memory[tile_start + vram_offset];
        uint8_t line_l = state.memory[tile_start + vram_offset + 1];

        vram_offset += 2;
        for (int pixel = 7; pixel >= 0; pixel--) {
          uint8_t w2 = 7 - pixel;
          bool bit_h = (line_h & (1 << pixel));
          bool bit_l = (line_l & (1 << pixel));
          uint8_t color_bits;
          if (bit_h && bit_l) color_bits = 3;
          if (!bit_h && bit_l) color_bits = 2;
          if (bit_h && !bit_l) color_bits = 1;
          if (!bit_h && !bit_l) color_bits = 0;
          window.at<uchar>(top + h2, left + w2) = color_bits;
        }
      }
      tileMapCounter++;
    }
  }
}



void renderFrame() {
  fillBackground();
  copyBackgroundtoScreen();
  if ((state.memory[0xff40] & (1 << 5)) != 0) { // Is the window on?
    fillWindow();
    copyWindowToScreen();
  }
  fillSprites();
}

void setScrollRegs() {
  state.scrollY = state.memory[0xFF42];
  state.scrollX = state.memory[0xFF43];
  state.windowY = state.memory[0xFF4A];
  state.windowX = state.memory[0xFF4B];
}

pair<int,int> seperateBytes(uint16_t word) {
  return pair<int,int> { (word >> 8), (word & 0xFF) };
}

uint8_t convertFlagsToByte() {
  uint8_t byte = 0;
  if (state.flags.z) byte += 128;
  if (state.flags.n) byte += 64;
  if (state.flags.h) byte += 32;
  if (state.flags.cy) byte += 16;
  return byte;
}

void restoreFlagsFromByte(uint8_t byte) {
  if (byte > 127) { state.flags.z = true; byte -= 128; }
  else { state.flags.z = false; }
  if (byte > 63) { state.flags.n = true; byte -= 64; }
  else { state.flags.n = false; }
  if (byte > 31) { state.flags.h = true; byte -= 32; }
  else { state.flags.h = false; }
  if (byte > 15) { state.flags.cy = true; }
  else { state.flags.cy = false; }
}


void setFlags(int a, int b, int setZ, int setN, int setH, int setCY) {
  if (setZ == 9) state.flags.z = (a == 0) || (a == 1);
  else if (setZ == 8) state.flags.z = ((a == 0) || (a == 1)) && !state.flags.cy;
  else if (setZ == 7) state.flags.z = ((a | b) == 0);
  else if (setZ == 6) state.flags.z = ((a == 0) || (a == 128)) && !state.flags.cy;
  else if (setZ == 5) state.flags.z = ((a & b) == 0); //AND
  else if (setZ == 4) state.flags.z = ((a ^ b) == 0); //XOR
  else if (setZ == 3) state.flags.z = (a + b == 0) || (a + b == 256); //addition
  else if (setZ == 2) state.flags.z = (a - b == 0); // subtraction
  else if (setZ == 1 || setZ == 0) state.flags.z = setZ;

  if (setN == 1 || setN == 0) state.flags.n = setN;

  if (setH == 6) state.flags.h = ((a & 0xf) - (b & 0xf) - state.flags.cy) < 0; // Subtraction with carry
  else if (setH == 5) state.flags.h = ((a & 0xf) + (b & 0xf) + state.flags.cy) > 0xf; // Addition with carry
  else if (setH == 4) state.flags.h = ((a & 0xfff) + (b & 0xfff)) > 0xfff; // Addition 16bit
  else if (setH == 3) state.flags.h = ((a & 0xf) + (b & 0xf)) > 0xf; // Addition
  else if (setH == 2) state.flags.h = ((a & 0xf) - (b & 0xf)) < 0; //Subtraction
  else if (setH == 1 || setH == 0) state.flags.h = setH;

  if (setCY == 8) state.flags.cy = (a - b - state.flags.cy) < 0;  // Subtraction with carry
  else if (setCY == 7) state.flags.cy = (a + b + state.flags.cy) > 255;  // Addition with carry
  else if (setCY == 6) state.flags.cy = (a % 2 != 0); // Odd or even
  else if (setCY == 5) state.flags.cy = (a + b) > 65535;  // Addition 16bit
  else if (setCY == 4) state.flags.cy = (a + b) > 255;  // Addition
  else if (setCY == 3) state.flags.cy = a > 127;
  else if (setCY == 2) state.flags.cy = (a < b);  // Subtraction
  else if (setCY == 1 || setCY == 0) state.flags.cy = setCY;
}

void processButtons() {
  state.directionInputBits = 15; state.buttonInputBits = 15;
  if (buttons.right) { state.directionInputBits -= 1; }
  if (buttons.left) { state.directionInputBits -= 2; }
  if (buttons.up) { state.directionInputBits -= 4; }
  if (buttons.down) { state.directionInputBits -= 8; }

  if (buttons.start) { state.buttonInputBits -= 8; }
  if (buttons.select) { state.buttonInputBits -= 4; }
  if (buttons.b) { state.buttonInputBits -= 2; }
  if (buttons.a) { state.buttonInputBits -= 1; }

  if (state.directionInputBits != 15 || state.buttonInputBits != 15) state.halted = false;

}

void updateButtons(bool pressed, int key) {
  switch (key) {
    case SDLK_UP: buttons.up = pressed; break;
    case SDLK_DOWN: buttons.down = pressed; break;
    case SDLK_LEFT: buttons.left = pressed; break;
    case SDLK_RIGHT: buttons.right = pressed; break;
    case SDLK_RETURN: buttons.start = pressed; break;
    case SDLK_RSHIFT: buttons.select = pressed; break;
    case SDLK_x: buttons.a = pressed; break;
    case SDLK_c: buttons.b = pressed; break;
    case SDLK_l: if (pressed) state.importStateFromFile(); break;  /// Load State
    case SDLK_s: if (pressed) state.writeStateToFile(); break;  /// Save State
    default: return;
  }
}

void process_sdl_events() {
  SDL_Event event;

  while (SDL_PollEvent(&event)) {
    switch (event.type) {
      case SDL_KEYDOWN:
        if (event.key.repeat == true) break;
        updateButtons(true, event.key.keysym.sym); break;
      case SDL_KEYUP:
        if (event.key.repeat == true) break;
        updateButtons(false, event.key.keysym.sym); break;
      case SDL_WINDOWEVENT: if (event.window.event == SDL_WINDOWEVENT_CLOSE) stop = true; break;
      case SDL_QUIT: stop = true; break;
    }
  }
}

void copyMatToSDL(uint32_t* pixels){
  for (int h = 0; h < 288; h++) {
    for (int w = 0; w < 320; w++) {
      uint8_t c = screen.at<Vec3b>(h,w)[0];
      uint32_t colorRGB = (c << 16) | (c << 8) | (c << 0);
      pixels[(320 * h) + w] = colorRGB;
    }
  }
}

void drawSDL(){
  process_sdl_events();

  SDL_RenderClear(renderer);

  void* pixels_ptr;
  int pitch;
  SDL_LockTexture(gb_screen_texture, nullptr, &pixels_ptr, &pitch);

  uint32_t* pixels = static_cast<uint32_t*>(pixels_ptr);
  copyMatToSDL(pixels);
  SDL_UnlockTexture(gb_screen_texture);

  SDL_RenderCopy(renderer, gb_screen_texture, nullptr, nullptr);
  SDL_RenderPresent(renderer);
}

void videoPipeline() {
  processButtons();
  setScrollRegs();
  renderFrame();
  drawSDL();
}


void pseudoVideoLineCounter(uint64_t cycles) {
  static uint64_t last_inc = 0;

  if (cycles > last_inc + 900) {  /// 456) {
    last_inc = cycles;
    state.memory[0xff44]++; //inc LY
    if (state.memory[0xff44] == 154) state.memory[0xff44] = 0;

    if (state.memory[0xff44] == 144) {
      state.memory[0xff0f] = 1; ///Set the first bit in the interrupt flag
      state.memory[0xff04]++; /// Incrementing Special Register for PRNG
      videoPipeline();
    }

  }
}


void overwriteBootRom() {
  for (int i = 0; i < 256; i++) {
    state.memory[i] = initCartRom[i];
  }
}

void initMats() {
  background = Mat(256,256,CV_8UC1);
  window = Mat(256,256,CV_8UC1);
  for (int h = 0; h < 256; h++) {
    for (int w = 0; w < 256; w++) {
      background.at<Vec3b>(h,w) = Vec3b(255,255,255);
    }
  }
  screen = Mat(288,320,CV_8UC3);
}

void initRomData(char* romLocation) {
  fstream romData(romLocation, ios::in | ios::binary);
  if (!romData) { cout << "Error: Could not load ROM" << endl; exit(0); }
  strncpy(state.game_name, romLocation, strlen(romLocation));
  int byte = romData.get();
  for (int i = 0; i <= 0xFFFF || byte != -1; i++) {
    if (i < 256) {  /// bootROM
      initCartRom[i] = byte;
      byte = romData.get();
      state.memory[i] = bootRom[i];
    } else if (i < 0x8000) {  // main ROM
      state.memory[i] = byte;
      byte = romData.get();
    } else if (i <= 0xFFFF) {  // RAM / VRAM / Special
      state.memory[i] = 0;
    } else {  /// Extended ROM
      state.memory[i] = byte;
      byte = romData.get();
    }
  }

  if (state.memory[0x147] >= 1) state.cartIsMBCType = true;
}

void interrupt() {
  state.halted = false;
  state.memory[extendAddr(state.sp - 1)] = seperateBytes(state.pc).first;
  state.memory[extendAddr(state.sp - 2)] = seperateBytes(state.pc).second;
  state.sp -= 2;

  state.memory[0xff0f] = 0; // Reset Interrupt Flag
  state.pc = 0x40; // Jumping to vBlank interrupt

  state.interrupt_enable = false;
}

void initSDL() {
  SDL_Init(SDL_INIT_VIDEO);

  sdl_window = SDL_CreateWindow("Gameboy Emulator", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 320, 288, SDL_WINDOW_OPENGL);
  if (sdl_window == nullptr) { cout << "Failed to initialise SDL" << endl; exit(-1); }

  renderer = SDL_CreateRenderer(sdl_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  gb_screen_texture = SDL_CreateTexture(renderer,  SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, 320, 288);
}

void shutdownSDL()  {
  SDL_DestroyTexture(gb_screen_texture);
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(sdl_window);
  SDL_Quit();
}


int main(int argc, char *argv[]) {

  if (argc != 2) { cout << "Usage: ./gameboy path_to_rom.gb" << endl; exit(0); }

  initRomData(argv[1]);
  initMats();
  initSDL();

  uint64_t last_draw = 0;

  while(!stop) {

    pseudoVideoLineCounter(state.cycles);
    if (state.interrupt_enable && ((state.memory[0xffff] & state.memory[0xff0f]) != 0)) interrupt();

    if (state.halted) { state.cycles++; continue; }

    stop |= !processOpcode(&state);
  }

  shutdownSDL();
  return 0;
}