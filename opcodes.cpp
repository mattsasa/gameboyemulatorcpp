#include "gameboy.h"

bool unimplementedOpcode(int opcode) {
  cout << "Error Unimplemented Opcode: " << std::hex << opcode << endl;
  return false;
}

bool processOpcode(ProcessorState *state) {
  uint8_t opcode = state->memory[extendAddr(state->pc)];
  uint8_t arg1 = state->memory[extendAddr(state->pc + 1)];
  uint8_t arg2 = state->memory[extendAddr(state->pc + 2)];

  state->pc++;

  uint8_t a, b, c, d, e, h, l;
  uint16_t bc, de, hl;
  uint8_t value = 0, value2 = 0;
  uint16_t addr, value16;
  pair<int,int> bytePair;
  bool boolean;

  switch (opcode) {
    case 0x00: break;
    case 0x01:
      bytePair = seperateBytes((arg2 * 256) + arg1); state->b = bytePair.first;
      state->c = bytePair.second; state->pc += 2; state->cycles += 12; break;
    case 0x02:
      addr = (state->b * 256) + state->c; if (!isRom(addr)) state->memory[extendAddr(addr)] = state->a;
      state->cycles += 8; break;
    case 0x03:
      state->c++; if (state->c == 0) state->b++; state->cycles += 8; break;
    case 0x04: setFlags(state->b, 1, 3, 0, 3, -1); state->b++; state->cycles += 4; break;
    case 0x05: setFlags(state->b, 1, 2, 1, 2, -1); state->b--; state->cycles += 4; break;
    case 0x06: state->b = arg1; state->pc++; state->cycles += 8; break;
    case 0x07: //RLC A
      boolean = (state->a > 127); state->a = seperateBytes(state->a * 2).second;
      if (boolean) state->a++; setFlags(state->a, 0, 0, 0, 0, boolean); state->cycles += 8; break;
    case 0x08:
      state->pc += 2; addr = (arg2 * 256) + arg1;  bytePair = seperateBytes(state->sp);
      if (!isRom(addr)) state->memory[extendAddr(addr)] = bytePair.second;
      if (!isRom(addr + 1)) state->memory[extendAddr(addr + 1)] = bytePair.first;
      state->cycles += 20; break;
    case 0x09:
      hl = (state->h * 256) + state->l; bc = (state->b * 256) + state->c; setFlags(hl, bc, -1, 0, 4, 5);
      hl += bc; bytePair = seperateBytes(hl); state->h = bytePair.first;
      state->l = bytePair.second; state->cycles += 8; break;
    case 0x0a:
      addr = (state->b * 256) + state->c; state->a = state->memory[extendAddr(addr)]; state->cycles += 8; break;
    case 0x0b:
      state->c--; if (state->c == 255) state->b--; state->cycles += 8; break;
    case 0x0c: setFlags(state->c, 1, 3, 0, 3, -1); state->c++; state->cycles += 4; break;
    case 0x0d: setFlags(state->c, 1, 2, 1, 2, -1); state->c--; state->cycles += 4; break;
    case 0x0e: state->c = arg1; state->pc++; state->cycles += 8; break;
    case 0x0f:
      boolean = (state->a % 2 != 0); setFlags(state->a, 0, 0, 0, 0, 6); state->a = floor(state->a / 2);
      if (boolean) state->a += 128; state->cycles += 8; break;
    case 0x10: return false; // STOP
    case 0x11:
      bytePair = seperateBytes((arg2 * 256) + arg1); state->d = bytePair.first;
      state->e = bytePair.second; state->pc += 2; state->cycles += 12; break;
    case 0x12:
      addr = (state->d * 256) + state->e; if (!isRom(addr)) state->memory[extendAddr(addr)] = state->a;
      state->cycles += 8; break;
    case 0x13:
      state->e++; if (state->e == 0) state->d++; state->cycles += 8; break;
    case 0x14: setFlags(state->d, 1, 3, 0, 3, -1); state->d++; state->cycles += 4; break;
    case 0x15: setFlags(state->d, 1, 2, 1, 2, -1); state->d--; state->cycles += 4; break;
    case 0x16: state->d = arg1; state->pc++; state->cycles += 8; break;
    case 0x17:
      boolean = state->flags.cy; setFlags(state->a, 0, 0, 0, 0, 3); value16 = state->a * 2;
      state->a = seperateBytes(value16).second; if (boolean) state->a++; state->cycles += 8; break;
    case 0x18:
      state->pc++; state->pc += (int8_t)arg1; state->cycles += 8; break;
    case 0x19: // HL += DE
      hl = (state->h * 256) + state->l; de = (state->d * 256) + state->e; setFlags(hl, de, -1, 0, 4, 5);
      hl += de; bytePair = seperateBytes(hl); state->h = bytePair.first;
      state->l = bytePair.second; state->cycles += 8; break;
    case 0x1a:
      addr = (state->d * 256) + state->e; state->a = state->memory[extendAddr(addr)]; state->cycles += 8; break;
    case 0x1b:
      state->e--; if (state->e == 255) state->d--; state->cycles += 8; break;
    case 0x1c: setFlags(state->e, 1, 3, 0, 3, -1); state->e++; state->cycles += 4; break;
    case 0x1d: setFlags(state->e, 1, 2, 1, 2, -1); state->e--; state->cycles += 4; break;
    case 0x1e: state->e = arg1; state->pc++; state->cycles += 8; break;
    case 0x1f: //RR A
      boolean = state->flags.cy; setFlags(state->a, 0, 0, 0, 0, 6); state->a = floor(state->a / 2);
      if (boolean) state->a += 128; state->cycles += 8; break;
    case 0x20:
      state->pc++; if (!state->flags.z) state->pc += (int8_t)arg1;
      state->cycles += 4; break;
    case 0x21:
      bytePair = seperateBytes((arg2 * 256) + arg1); state->h = bytePair.first;
      state->l = bytePair.second; state->pc += 2; state->cycles += 12; break;
    case 0x22:
      addr = (state->h * 256) + state->l; if (!isRom(addr)) state->memory[extendAddr(addr)] = state->a;
      state->l++; if (state->l == 0) state->h++; state->cycles += 8; break;
    case 0x23:
      state->l++; if (state->l == 0) state->h++; state->cycles += 8; break;
    case 0x24: setFlags(state->h, 1, 3, 0, 3, -1); state->h++; state->cycles += 4; break;
    case 0x25: setFlags(state->h, 1, 2, 1, 2, -1); state->h--; state->cycles += 4; break;
    case 0x26: state->h = arg1; state->pc++; state->cycles += 8; break;
    case 0x27: //DAA Decimal Adjust A
      boolean = state->flags.cy || (!state->flags.n && (state->a > 0x99));
      if (!state->flags.n) {  // Addition
        if (state->flags.h || ((state->a & 0x0f) > 9)) value += 0x06; /// Add 6 to lower_nibble
        if (state->flags.cy || (state->a > 0x99)) value += 0x60;   // Add 6 to upper_nibble
        state->a += value;
      } else {  // Subtraction
        if (state->flags.h)  state->a -= 0x06; /// Add 6 to lower_nibble
        if (state->flags.cy) state->a -= 0x60;   // Add 6 to upper_nibble
      }
      setFlags(state->a, 0, 2, -1, 0, boolean); state->cycles += 16; break;
    case 0x28:
      state->pc++; if (state->flags.z) state->pc += (int8_t)arg1;
      state->cycles += 8; break;
    case 0x29:
      hl = (state->h * 256) + state->l; setFlags(hl, hl, -1, 0, 4, 5);
      hl += hl; bytePair = seperateBytes(hl); state->h = bytePair.first;
      state->l = bytePair.second; state->cycles += 8; break;
    case 0x2a:
      addr = (state->h * 256) + state->l; state->a = state->memory[extendAddr(addr)];
      state->l++; if (state->l == 0) state->h++; state->cycles += 8; break;
    case 0x2b:
      state->l--; if (state->l == 255) state->h--; state->cycles += 8; break;
    case 0x2c: setFlags(state->l, 1, 3, 0, 3, -1); state->l++; state->cycles += 4; break;
    case 0x2d: setFlags(state->l, 1, 2, 1, 2, -1); state->l--; state->cycles += 4; break;
    case 0x2e: state->l = arg1; state->pc++; state->cycles += 8; break;
    case 0x2f: setFlags(0, 0, -1, 1, 1, -1); state->a = (uint8_t)~state->a; state->cycles += 4; break;
    case 0x30:
      state->pc++; if (!state->flags.cy) state->pc += (int8_t)arg1;
      state->cycles += 8; break;
    case 0x31:
      state->sp = (arg2 * 256) + arg1; state->pc += 2; state->cycles += 12; break;
    case 0x32:
      addr = (state->h * 256) + state->l; if (!isRom(addr)) state->memory[extendAddr(addr)] = state->a;
      state->l--; if (state->l == 255) state->h--; state->cycles += 8; break;
    case 0x33:
      state->sp++; state->cycles += 8; break;
    case 0x34:
      addr = (state->h * 256) + state->l; setFlags(state->memory[extendAddr(addr)], 1, 3, 0, 3, -1);
      state->memory[extendAddr(addr)]++; state->cycles += 12; break;
    case 0x35:
      addr = (state->h * 256) + state->l; setFlags(state->memory[extendAddr(addr)], 1, 2, 1, 2, -1);
      state->memory[extendAddr(addr)]--; state->cycles += 12; break;
    case 0x36:
      addr = (state->h * 256) + state->l;
      if (addr >= 0x2000 && addr <= 0x3FFF && state->cartIsMBCType) { ///Setting ROM Mode
        if (arg1 == 1) { state->romBankMode = 0; }
        else { state->romBankMode = arg1 & 0x7F; }
      }
      else if (!isRom(addr)) state->memory[extendAddr(addr)] = arg1;
      state->pc++; state->cycles += 12; break;
    case 0x37:
      setFlags(0, 0, -1, 0, 0, 1); state->cycles += 4; break;
    case 0x38:
      state->pc++; if (state->flags.cy) state->pc += (int8_t)arg1;
      state->cycles += 8; break;
    case 0x39:
      hl = (state->h * 256) + state->l; setFlags(hl, state->sp, -1, 0, 4, 5);
      hl += state->sp; bytePair = seperateBytes(hl); state->h = bytePair.first;
      state->l = bytePair.second; state->cycles += 8; break;
    case 0x3a:
      addr = (state->h * 256) + state->l; state->a = state->memory[extendAddr(addr)];
      state->l--; if (state->l == 255) state->h--; state->cycles += 8; break;
    case 0x3b:
      state->sp--; state->cycles += 8; break;
    case 0x3c: setFlags(state->a, 1, 3, 0, 3, -1); state->a++; state->cycles += 4; break;
    case 0x3d: setFlags(state->a, 1, 2, 1, 2, -1); state->a--; state->cycles += 4; break;
    case 0x3e: state->a = arg1; state->pc++; state->cycles += 8; break;
    case 0x3f: setFlags(!state->flags.cy, 0, -1, 0, 0, 6); state->cycles += 4; break;

    case 0x40: state->b = state->b; state->cycles += 4; break;
    case 0x41: state->b = state->c; state->cycles += 4; break;
    case 0x42: state->b = state->d; state->cycles += 4; break;
    case 0x43: state->b = state->e; state->cycles += 4; break;
    case 0x44: state->b = state->h; state->cycles += 4; break;
    case 0x45: state->b = state->l; state->cycles += 4; break;
    case 0x46: addr = (state->h * 256) + state->l; state->b = state->memory[extendAddr(addr)]; state->cycles += 8; break;
    case 0x47: state->b = state->a; state->cycles += 4; break;

    case 0x48: state->c = state->b; state->cycles += 4; break;
    case 0x49: state->c = state->c; state->cycles += 4; break;
    case 0x4a: state->c = state->d; state->cycles += 4; break;
    case 0x4b: state->c = state->e; state->cycles += 4; break;
    case 0x4c: state->c = state->h; state->cycles += 4; break;
    case 0x4d: state->c = state->l; state->cycles += 4; break;
    case 0x4e: addr = (state->h * 256) + state->l; state->c = state->memory[extendAddr(addr)]; state->cycles += 8; break;
    case 0x4f: state->c = state->a; state->cycles += 4; break;

    case 0x50: state->d = state->b; state->cycles += 4; break;
    case 0x51: state->d = state->c; state->cycles += 4; break;
    case 0x52: state->d = state->d; state->cycles += 4; break;
    case 0x53: state->d = state->e; state->cycles += 4; break;
    case 0x54: state->d = state->h; state->cycles += 4; break;
    case 0x55: state->d = state->l; state->cycles += 4; break;
    case 0x56: addr = (state->h * 256) + state->l; state->d = state->memory[extendAddr(addr)]; state->cycles += 8; break;
    case 0x57: state->d = state->a; state->cycles += 4; break;

    case 0x58: state->e = state->b; state->cycles += 4; break;
    case 0x59: state->e = state->c; state->cycles += 4; break;
    case 0x5a: state->e = state->d; state->cycles += 4; break;
    case 0x5b: state->e = state->e; state->cycles += 4; break;
    case 0x5c: state->e = state->h; state->cycles += 4; break;
    case 0x5d: state->e = state->l; state->cycles += 4; break;
    case 0x5e: addr = (state->h * 256) + state->l; state->e = state->memory[extendAddr(addr)]; state->cycles += 8; break;
    case 0x5f: state->e = state->a; state->cycles += 4; break;

    case 0x60: state->h = state->b; state->cycles += 4; break;
    case 0x61: state->h = state->c; state->cycles += 4; break;
    case 0x62: state->h = state->d; state->cycles += 4; break;
    case 0x63: state->h = state->e; state->cycles += 4; break;
    case 0x64: state->h = state->h; state->cycles += 4; break;
    case 0x65: state->h = state->l; state->cycles += 4; break;
    case 0x66: addr = (state->h * 256) + state->l; state->h = state->memory[extendAddr(addr)]; state->cycles += 8; break;
    case 0x67: state->h = state->a; state->cycles += 4; break;

    case 0x68: state->l = state->b; state->cycles += 4; break;
    case 0x69: state->l = state->c; state->cycles += 4; break;
    case 0x6a: state->l = state->d; state->cycles += 4; break;
    case 0x6b: state->l = state->e; state->cycles += 4; break;
    case 0x6c: state->l = state->h; state->cycles += 4; break;
    case 0x6d: state->l = state->l; state->cycles += 4; break;
    case 0x6e: addr = (state->h * 256) + state->l; state->l = state->memory[extendAddr(addr)]; state->cycles += 8; break;
    case 0x6f: state->l = state->a; state->cycles += 4; break;

    case 0x70: addr = (state->h * 256) + state->l; if (!isRom(addr)) state->memory[extendAddr(addr)] = state->b;
      state->cycles += 8; break;
    case 0x71: addr = (state->h * 256) + state->l; if (!isRom(addr)) state->memory[extendAddr(addr)] = state->c;
      state->cycles += 8; break;
    case 0x72: addr = (state->h * 256) + state->l; if (!isRom(addr)) state->memory[extendAddr(addr)] = state->d;
      state->cycles += 8; break;
    case 0x73: addr = (state->h * 256) + state->l; if (!isRom(addr)) state->memory[extendAddr(addr)] = state->e;
      state->cycles += 8; break;
    case 0x74: addr = (state->h * 256) + state->l; if (!isRom(addr)) state->memory[extendAddr(addr)] = state->h;
      state->cycles += 8; break;
    case 0x75: addr = (state->h * 256) + state->l; if (!isRom(addr)) state->memory[extendAddr(addr)] = state->l;
      state->cycles += 8; break;

    case 0x76: /// HALT
      state->halted = true; state->cycles += 4; break;
    case 0x77:
      addr = (state->h * 256) + state->l; if (!isRom(addr)) state->memory[extendAddr(addr)] = state->a;
      state->cycles += 8; break;

    case 0x78: state->a = state->b; state->cycles += 4; break;
    case 0x79: state->a = state->c; state->cycles += 4; break;
    case 0x7a: state->a = state->d; state->cycles += 4; break;
    case 0x7b: state->a = state->e; state->cycles += 4; break;
    case 0x7c: state->a = state->h; state->cycles += 4; break;
    case 0x7d: state->a = state->l; state->cycles += 4; break;
    case 0x7e: addr = (state->h * 256) + state->l; state->a = state->memory[extendAddr(addr)]; state->cycles += 8; break;
    case 0x7f: state->a = state->a; state->cycles += 4; break;

    case 0x80:
      setFlags(state->a, state->b, 3, 0, 3, 4); state->a += state->b; state->cycles += 4; break;
    case 0x81:
      setFlags(state->a, state->c, 3, 0, 3, 4); state->a += state->c; state->cycles += 4; break;
    case 0x82:
      setFlags(state->a, state->d, 3, 0, 3, 4); state->a += state->d; state->cycles += 4; break;
    case 0x83:
      setFlags(state->a, state->e, 3, 0, 3, 4); state->a += state->e; state->cycles += 4; break;
    case 0x84:
      setFlags(state->a, state->h, 3, 0, 3, 4); state->a += state->h; state->cycles += 4; break;
    case 0x85:
      setFlags(state->a, state->l, 3, 0, 3, 4); state->a += state->l; state->cycles += 4; break;
    case 0x86:
      addr = (state->h * 256) + state->l; setFlags(state->a, state->memory[extendAddr(addr)], 3, 0, 3, 4);
      state->a += state->memory[extendAddr(addr)]; state->cycles += 8; break;
    case 0x87:
      setFlags(state->a, state->a, 3, 0, 3, 4); state->a += state->a; state->cycles += 4; break;


    case 0x88:
      boolean = state->flags.cy; setFlags(state->a, state->b, -1,-1,5,7);
      state->a = state->a + state->b + boolean; setFlags(state->a,0,3,0,-1,-1); state->cycles += 4; break;
    case 0x89:
      boolean = state->flags.cy; setFlags(state->a, state->c, -1,-1,5,7);
      state->a = state->a + state->c + boolean; setFlags(state->a,0,3,0,-1,-1); state->cycles += 4; break;
    case 0x8a:
      boolean = state->flags.cy; setFlags(state->a, state->d, -1,-1,5,7);
      state->a = state->a + state->d + boolean; setFlags(state->a,0,3,0,-1,-1); state->cycles += 4; break;
    case 0x8b:
      boolean = state->flags.cy; setFlags(state->a, state->e, -1,-1,5,7);
      state->a = state->a + state->e + boolean; setFlags(state->a,0,3,0,-1,-1); state->cycles += 4; break;
    case 0x8c:
      boolean = state->flags.cy; setFlags(state->a, state->h, -1,-1,5,7);
      state->a = state->a + state->h + boolean; setFlags(state->a,0,3,0,-1,-1); state->cycles += 4; break;
    case 0x8d:
      boolean = state->flags.cy; setFlags(state->a, state->l, -1,-1,5,7);
      state->a = state->a + state->l + boolean; setFlags(state->a,0,3,0,-1,-1); state->cycles += 4; break;
    case 0x8e:
      addr = (state->h * 256) + state->l; boolean = state->flags.cy;
      setFlags(state->a, state->memory[extendAddr(addr)], -1,-1,5,7);
      state->a = state->a + state->memory[extendAddr(addr)] + boolean;
      setFlags(state->a,0,3,0,-1,-1); state->cycles += 8; break;
    case 0x8f:
      boolean = state->flags.cy; setFlags(state->a, state->a, -1,-1,5,7);
      state->a = state->a + state->a + boolean; setFlags(state->a,0,3,0,-1,-1); state->cycles += 4; break;

    case 0x90:
      setFlags(state->a, state->b, 2, 1, 2, 2); state->a -= state->b; state->cycles += 4; break;
    case 0x91:
      setFlags(state->a, state->c, 2, 1, 2, 2); state->a -= state->c; state->cycles += 4; break;
    case 0x92:
      setFlags(state->a, state->d, 2, 1, 2, 2); state->a -= state->d; state->cycles += 4; break;
    case 0x93:
      setFlags(state->a, state->e, 2, 1, 2, 2); state->a -= state->e; state->cycles += 4; break;
    case 0x94:
      setFlags(state->a, state->h, 2, 1, 2, 2); state->a -= state->h; state->cycles += 4; break;
    case 0x95:
      setFlags(state->a, state->l, 2, 1, 2, 2); state->a -= state->l; state->cycles += 4; break;
    case 0x96:
      addr = (state->h * 256) + state->l; setFlags(state->a, state->memory[extendAddr(addr)], 2, 1, 2, 2);
      state->a -= state->memory[extendAddr(addr)]; state->cycles += 8; break;
    case 0x97:
      setFlags(state->a, state->a, 2, 1, 2, 2); state->a -= state->a; state->cycles += 4; break;

    case 0x98:
      boolean = state->flags.cy; setFlags(state->a, state->b, -1,-1,6,8);
      state->a = state->a - state->b - boolean; setFlags(state->a, 0, 3,1,-1,-1); state->cycles += 4; break;
    case 0x99:
      boolean = state->flags.cy; setFlags(state->a, state->c, -1,-1,6,8);
      state->a = state->a - state->c - boolean; setFlags(state->a, 0, 3,1,-1,-1); state->cycles += 4; break;
    case 0x9a:
      boolean = state->flags.cy; setFlags(state->a, state->d, -1,-1,6,8);
      state->a = state->a - state->d - boolean; setFlags(state->a, 0, 3,1,-1,-1); state->cycles += 4; break;
    case 0x9b:
      boolean = state->flags.cy; setFlags(state->a, state->e, -1,-1,6,8);
      state->a = state->a - state->e - boolean; setFlags(state->a, 0, 3,1,-1,-1); state->cycles += 4; break;
    case 0x9c:
      boolean = state->flags.cy; setFlags(state->a, state->h, -1,-1,6,8);
      state->a = state->a - state->h - boolean; setFlags(state->a, 0, 3,1,-1,-1); state->cycles += 4; break;
    case 0x9d:
      boolean = state->flags.cy; setFlags(state->a, state->l, -1,-1,6,8);
      state->a = state->a - state->l - boolean; setFlags(state->a, 0, 3,1,-1,-1); state->cycles += 4; break;
    case 0x9e:
      addr = (state->h * 256) + state->l;
      boolean = state->flags.cy; setFlags(state->a, state->memory[extendAddr(addr)], -1,-1,6,8);
      state->a = state->a - state->memory[extendAddr(addr)] - boolean;
      setFlags(state->a, 0, 3,1,-1,-1); state->cycles += 8; break;
    case 0x9f:
      boolean = state->flags.cy; setFlags(state->a, state->a, -1,-1,6,8);
      state->a = state->a - state->a - boolean; setFlags(state->a, 0, 3,1,-1,-1); state->cycles += 4; break;

    case 0xa0:
      setFlags(state->a, state->b, 5, 0, 1, 0); state->a = state->a & state->b; state->cycles += 4; break;
    case 0xa1:
      setFlags(state->a, state->c, 5, 0, 1, 0); state->a = state->a & state->c; state->cycles += 4; break;
    case 0xa2:
      setFlags(state->a, state->d, 5, 0, 1, 0); state->a = state->a & state->d; state->cycles += 4; break;
    case 0xa3:
      setFlags(state->a, state->e, 5, 0, 1, 0); state->a = state->a & state->e; state->cycles += 4; break;
    case 0xa4:
      setFlags(state->a, state->h, 5, 0, 1, 0); state->a = state->a & state->h; state->cycles += 4; break;
    case 0xa5:
      setFlags(state->a, state->l, 5, 0, 1, 0); state->a = state->a & state->l; state->cycles += 4; break;
    case 0xa6:
      addr = (state->h * 256) + state->l; setFlags(state->a, state->memory[extendAddr(addr)], 5, 0, 1, 0);
      state->a = state->a & state->memory[extendAddr(addr)]; state->cycles += 8; break;
    case 0xa7:
      setFlags(state->a, state->a, 5, 0, 1, 0); state->a = state->a & state->a; state->cycles += 4; break;

    case 0xa8:
      setFlags(state->a, state->b, 4, 0, 0, 0); state->a = state->a ^ state->b; state->cycles += 4; break;
    case 0xa9:
      setFlags(state->a, state->c, 4, 0, 0, 0); state->a = state->a ^ state->c; state->cycles += 4; break;
    case 0xaa:
      setFlags(state->a, state->d, 4, 0, 0, 0); state->a = state->a ^ state->d; state->cycles += 4; break;
    case 0xab:
      setFlags(state->a, state->e, 4, 0, 0, 0); state->a = state->a ^ state->e; state->cycles += 4; break;
    case 0xac:
      setFlags(state->a, state->h, 4, 0, 0, 0); state->a = state->a ^ state->h; state->cycles += 4; break;
    case 0xad:
      setFlags(state->a, state->l, 4, 0, 0, 0); state->a = state->a ^ state->l; state->cycles += 4; break;
    case 0xae:
      addr = (state->h * 256) + state->l; setFlags(state->a, state->memory[extendAddr(addr)], 4, 0, 0, 0);
      state->a = state->a ^ state->memory[extendAddr(addr)]; state->cycles += 4; break;
    case 0xaf:
      setFlags(state->a, state->a, 4, 0, 0, 0); state->a = state->a ^ state->a; state->cycles += 4; break;

    case 0xb0:
      setFlags(state->a, state->b, 7, 0, 0, 0); state->a = state->a | state->b; state->cycles += 4; break;
    case 0xb1:
      setFlags(state->a, state->c, 7, 0, 0, 0); state->a = state->a | state->c; state->cycles += 4; break;
    case 0xb2:
      setFlags(state->a, state->d, 7, 0, 0, 0); state->a = state->a | state->d; state->cycles += 4; break;
    case 0xb3:
      setFlags(state->a, state->e, 7, 0, 0, 0); state->a = state->a | state->e; state->cycles += 4; break;
    case 0xb4:
      setFlags(state->a, state->h, 7, 0, 0, 0); state->a = state->a | state->h; state->cycles += 4; break;
    case 0xb5:
      setFlags(state->a, state->l, 7, 0, 0, 0); state->a = state->a | state->l; state->cycles += 4; break;
    case 0xb6:
      addr = (state->h * 256) + state->l; setFlags(state->a, state->memory[extendAddr(addr)], 7, 0, 0, 0);
      state->a = state->a | state->memory[extendAddr(addr)]; state->cycles += 8; break;
    case 0xb7:
      setFlags(state->a, state->a, 7, 0, 0, 0); state->a = state->a | state->a; state->cycles += 4; break;

    case 0xb8:
      setFlags(state->a, state->b, 2, 1, 2, 2); state->cycles += 8; break;
    case 0xb9:
      setFlags(state->a, state->c, 2, 1, 2, 2); state->cycles += 8; break;
    case 0xba:
      setFlags(state->a, state->d, 2, 1, 2, 2); state->cycles += 8; break;
    case 0xbb:
      setFlags(state->a, state->e, 2, 1, 2, 2); state->cycles += 8; break;
    case 0xbc:
      setFlags(state->a, state->h, 2, 1, 2, 2); state->cycles += 8; break;
    case 0xbd:
      setFlags(state->a, state->l, 2, 1, 2, 2); state->cycles += 8; break;
    case 0xbe:
      addr = (state->h * 256) + state->l; setFlags(state->a, state->memory[extendAddr(addr)], 2, 1, 2, 2); state->cycles += 8; break;
    case 0xbf:
      setFlags(state->a, state->a, 2, 1, 2, 2); state->cycles += 8; break;

    case 0xc0:
      if (!state->flags.z) {
        state->pc = (state->memory[extendAddr(state->sp + 1)] * 256) + state->memory[extendAddr(state->sp)];
        state->sp += 2; state->cycles += 8; break;
      }
      state->cycles += 4; break;
    case 0xc1:
      state->b = state->memory[extendAddr(state->sp + 1)]; state->c = state->memory[extendAddr(state->sp)];
      state->sp += 2; state->cycles += 12; break;
    case 0xc2:
      !state->flags.z ? state->pc = (arg2 * 256) + arg1 : state->pc += 2;
      state->cycles += 12; break;
    case 0xc3:
      state->pc = (arg2 * 256) + arg1; state->cycles += 12; break;
    case 0xc4:
      if (!state->flags.z) {
        if (!isRom(state->sp - 1)) state->memory[extendAddr(state->sp - 1)] = seperateBytes(state->pc+2).first;
        if (!isRom(state->sp - 2)) state->memory[extendAddr(state->sp - 2)] = seperateBytes(state->pc+2).second;
        state->sp -= 2; state->pc = (arg2 * 256) + arg1;
      } else { state->pc += 2; } state->cycles += 12; break;
    case 0xc5:
      if (!isRom(state->sp - 2)) state->memory[extendAddr(state->sp - 2)] = state->c;
      if (!isRom(state->sp - 1)) state->memory[extendAddr(state->sp - 1)] = state->b;
      state->sp -= 2; state->cycles += 16; break;
    case 0xc6:
      state->pc++; setFlags(state->a, arg1, 3, 0, 3, 4); state->a += arg1; state->cycles += 8; break;
    case 0xc7:
      if (!isRom(state->sp - 1)) state->memory[extendAddr(state->sp - 1)] = seperateBytes(state->pc).first;
      if (!isRom(state->sp - 2)) state->memory[extendAddr(state->sp - 2)] = seperateBytes(state->pc).second;
      state->sp -= 2; state->pc = 0x00; state->cycles += 32; break;
    case 0xc8:
      if (state->flags.z) {
        state->pc = (state->memory[extendAddr(state->sp + 1)] * 256) + state->memory[extendAddr(state->sp)];
        state->sp += 2; state->cycles += 8; break;
      }
      state->cycles += 4; break;
    case 0xc9:
      state->pc = (state->memory[extendAddr(state->sp + 1)] * 256) + state->memory[extendAddr(state->sp)];
      state->sp += 2; state->cycles += 8; break;
    case 0xca:
      state->flags.z ? state->pc = (arg2 * 256) + arg1 : state->pc += 2;
      state->cycles += 12; break;
    case 0xcb: break; /// more OPCodes
    case 0xcc:
      if (state->flags.z) {
        if (!isRom(state->sp - 1)) state->memory[extendAddr(state->sp - 1)] = seperateBytes(state->pc+2).first;
        if (!isRom(state->sp - 2)) state->memory[extendAddr(state->sp - 2)] = seperateBytes(state->pc+2).second;
        state->sp -= 2; state->pc = (arg2 * 256) + arg1;
      } else { state->pc += 2; } state->cycles += 12; break;
    case 0xcd:
      if (!isRom(state->sp - 1)) state->memory[extendAddr(state->sp - 1)] = seperateBytes(state->pc+2).first;
      if (!isRom(state->sp - 2)) state->memory[extendAddr(state->sp - 2)] = seperateBytes(state->pc+2).second;
      state->sp -= 2; state->pc = (arg2 * 256) + arg1; state->cycles += 12; break;
    case 0xce:
      state->pc++; boolean = state->flags.cy; setFlags(state->a,arg1,-1,-1,5,7);
      state->a = state->a + boolean + arg1; setFlags(state->a,0,3,0,-1,-1); state->cycles += 8; break;
    case 0xcf:
      if (!isRom(state->sp - 1)) state->memory[extendAddr(state->sp - 1)] = seperateBytes(state->pc).first;
      if (!isRom(state->sp - 2)) state->memory[extendAddr(state->sp - 2)] = seperateBytes(state->pc).second;
      state->sp -= 2; state->pc = 0x08; state->cycles += 32; break;
    case 0xd0:
      if (!state->flags.cy) {
        state->pc = (state->memory[extendAddr(state->sp + 1)] * 256) + state->memory[extendAddr(state->sp)];
        state->sp += 2; state->cycles += 8; break;
      }
      state->cycles += 4; break;
    case 0xd1:
      state->d = state->memory[extendAddr(state->sp + 1)]; state->e = state->memory[extendAddr(state->sp)];
      state->sp += 2; state->cycles += 12; break;
    case 0xd2:
      !state->flags.cy ? state->pc = (arg2 * 256) + arg1 : state->pc += 2;
      state->cycles += 12; break;
    case 0xd4:
      if (!state->flags.cy) {
        if (!isRom(state->sp - 1)) state->memory[extendAddr(state->sp - 1)] = seperateBytes(state->pc+2).first;
        if (!isRom(state->sp - 2)) state->memory[extendAddr(state->sp - 2)] = seperateBytes(state->pc+2).second;
        state->sp -= 2; state->pc = (arg2 * 256) + arg1;
      } else { state->pc += 2; } state->cycles += 12; break;
    case 0xd5:
      if (!isRom(state->sp - 2)) state->memory[extendAddr(state->sp - 2)] = state->e;
      if (!isRom(state->sp - 1)) state->memory[extendAddr(state->sp - 1)] = state->d;
      state->sp -= 2; state->cycles += 16; break;
    case 0xd6:
      state->pc++; setFlags(state->a, arg1, 2, 1, 2, 2); state->a -= arg1; state->cycles += 8; break;
    case 0xd7:
      if (!isRom(state->sp - 1)) state->memory[extendAddr(state->sp - 1)] = seperateBytes(state->pc).first;
      if (!isRom(state->sp - 2)) state->memory[extendAddr(state->sp - 2)] = seperateBytes(state->pc).second;
      state->sp -= 2; state->pc = 0x10; state->cycles += 32; break;
    case 0xd8:
      if (state->flags.cy) {
        state->pc = (state->memory[extendAddr(state->sp + 1)] * 256) + state->memory[extendAddr(state->sp)];
        state->sp += 2; state->cycles += 8; break;
      }
      state->cycles += 4; break;
    case 0xd9: //RETI return interrupt
      state->pc = (state->memory[extendAddr(state->sp + 1)] * 256) + state->memory[extendAddr(state->sp)];
      state->sp += 2; state->interrupt_enable = true; state->cycles += 8; break;
    case 0xda:
      state->flags.cy ? state->pc = (arg2 * 256) + arg1 : state->pc += 2;
      state->cycles += 12; break;
    case 0xdc:
      if (state->flags.cy) {
        if (!isRom(state->sp - 1)) state->memory[extendAddr(state->sp - 1)] = seperateBytes(state->pc+2).first;
        if (!isRom(state->sp - 2)) state->memory[extendAddr(state->sp - 2)] = seperateBytes(state->pc+2).second;
        state->sp -= 2; state->pc = (arg2 * 256) + arg1;
      } else { state->pc += 2; } state->cycles += 12; break;
    case 0xde:
      state->pc++; boolean = state->flags.cy; setFlags(state->a,arg1,-1,-1,6,8);
      state->a = state->a - boolean - arg1; setFlags(state->a,0,3,1,-1,-1); state->cycles += 8; break;
    case 0xdf:
      if (!isRom(state->sp - 1)) state->memory[extendAddr(state->sp - 1)] = seperateBytes(state->pc).first;
      if (!isRom(state->sp - 2)) state->memory[extendAddr(state->sp - 2)] = seperateBytes(state->pc).second;
      state->sp -= 2; state->pc = 0x18; state->cycles += 32; break;
    case 0xe0:  // Write to special Registers
      if (arg1 == 0) state->memory[0xff00] = state->a + 15;  //Writing to special inputByte
      else if (arg1 == 0x46) initiateDMATransfer(state->a * 0x100);
      else if (arg1 == 0x50) overwriteBootRom();
      else state->memory[0xff00 + arg1] = state->a;
      state->pc++; state->cycles += 12; break;
    case 0xe1:
      state->h = state->memory[extendAddr(state->sp + 1)]; state->l = state->memory[extendAddr(state->sp)];
      state->sp += 2; state->cycles += 12; break;
    case 0xe2:
      state->memory[0xff00 + state->c] = state->a; state->cycles += 8; break;
    case 0xe5:
      if (!isRom(state->sp - 2)) state->memory[extendAddr(state->sp - 2)] = state->l;
      if (!isRom(state->sp - 1)) state->memory[extendAddr(state->sp - 1)] = state->h;
      state->sp -= 2; state->cycles += 16; break;
    case 0xe6:
      state->pc++; setFlags(state->a, arg1, 5, 0, 1, 0);
      state->a = state->a & arg1; state->cycles += 8; break;
    case 0xe7:
      if (!isRom(state->sp - 1)) state->memory[extendAddr(state->sp - 1)] = seperateBytes(state->pc).first;
      if (!isRom(state->sp - 2)) state->memory[extendAddr(state->sp - 2)] = seperateBytes(state->pc).second;
      state->sp -= 2; state->pc = 0x20; state->cycles += 32; break;
    case 0xe8:
      state->pc++; value16 = state->sp ^ (int8_t)arg1; state->sp += (int8_t)arg1;
      value = ((value16 ^ (state->sp & 0xFFFF)) & 0x10) == 0x10;
      value2 = ((value16 ^ (state->sp & 0xFFFF)) & 0x100) == 0x100;
      setFlags(0, 0, 0, 0, value, value2); state->cycles += 16; break;
    case 0xe9:
      addr = (state->h * 256) + state->l; state->pc = addr; state->cycles += 4; break;
    case 0xea:  //Includes writing to Rom Mode
      addr = (arg2 * 256) + arg1;
      if (addr >= 0x8000) if (!isRom(addr)) state->memory[extendAddr(addr)] = state->a;
      if (addr >= 0x2000 && addr <= 0x3FFF && state->cartIsMBCType) { ///Setting ROM Mode
        if (state->a == 1) { state->romBankMode = 0; }
        else { state->romBankMode = state->a & 0x7F; }
      }
      if (addr >= 0x4000 && addr <= 0x5FFF && state->cartIsMBCType) {
        if (state->a >= 0 && state->a <= 3) state->ramBankMode = state->a;
      }
      state->pc += 2; state->cycles += 16; break;
    case 0xee:
      state->pc++; setFlags(state->a, arg1, 4, 0, 0, 0); state->a = state->a ^ arg1; state->cycles += 8; break;
    case 0xef:
      if (!isRom(state->sp - 1)) state->memory[extendAddr(state->sp - 1)] = seperateBytes(state->pc).first;
      if (!isRom(state->sp - 2)) state->memory[extendAddr(state->sp - 2)] = seperateBytes(state->pc).second;
      state->sp -= 2; state->pc = 0x28; state->cycles += 32; break;

    case 0xf0: //Read from Special Registers / Read from input if arg1 == 0
      if (arg1 == 0 && state->memory[0xff00] == 31) state->a = 16 + state->buttonInputBits;
      else if (arg1 == 0 && state->memory[0xff00] == 47) state->a = 32 + state->directionInputBits;
      else state->a = state->memory[0xff00 + arg1];
      state->pc++; state->cycles += 4; break;

    case 0xf1:
      state->a = state->memory[extendAddr(state->sp + 1)]; value = state->memory[extendAddr(state->sp)];
      restoreFlagsFromByte(value); state->sp += 2; state->cycles += 12; break;
    case 0xf2:
      state->a = state->memory[0xff00 + state->c]; state->cycles += 8; break;
    case 0xf3: state->interrupt_enable = false; state->cycles += 4; break;
    case 0xf5:
      if (!isRom(state->sp - 2)) state->memory[extendAddr(state->sp - 2)] = convertFlagsToByte();
      if (!isRom(state->sp - 1)) state->memory[extendAddr(state->sp - 1)] = state->a;
      state->sp -= 2; state->cycles += 16; break;
    case 0xf6:
      state->pc++; setFlags(state->a, arg1, 7, 0, 0, 0); state->a = state->a | arg1; state->cycles += 8; break;
    case 0xf7:
      if (!isRom(state->sp - 1)) state->memory[extendAddr(state->sp - 1)] = seperateBytes(state->pc).first;
      if (!isRom(state->sp - 2)) state->memory[extendAddr(state->sp - 2)] = seperateBytes(state->pc).second;
      state->sp -= 2; state->pc = 0x30; state->cycles += 32; break;
    case 0xf8:
      state->pc++; value16 = state->sp ^ (int8_t)arg1; hl = state->sp + (int8_t)arg1;
      value = ((value16 ^ (hl & 0xFFFF)) & 0x10) == 0x10;
      value2 = ((value16 ^ (hl & 0xFFFF)) & 0x100) == 0x100;
      bytePair = seperateBytes(hl); state->h = bytePair.first; state->l = bytePair.second;
      setFlags(0, 0, 0, 0, value, value2); state->cycles += 16; break;
    case 0xf9:
      state->sp = (state->h * 256) + state->l; state->cycles += 8; break;
    case 0xfa:
      state->pc += 2; addr = (arg2 * 256) + arg1; state->a = state->memory[extendAddr(addr)];
      state->cycles += 16; break;
    case 0xfb: state->interrupt_enable = true; state->cycles += 4; break;
    case 0xfe:
      setFlags(state->a, arg1, 2, 1, 2, 2); state->cycles += 4; state->pc++; break;
    case 0xff:
      if (!isRom(state->sp - 1)) state->memory[extendAddr(state->sp - 1)] = seperateBytes(state->pc).first;
      if (!isRom(state->sp - 2)) state->memory[extendAddr(state->sp - 2)] = seperateBytes(state->pc).second;
      state->sp -= 2; state->pc = 0x38; state->cycles += 32; break;
    default: return unimplementedOpcode(opcode);
  }

  if (opcode != 0xcb) return true;  /// Normal endpoint

  uint8_t cb_opcode = state->memory[extendAddr(state->pc)];
  state->pc++;

  switch (cb_opcode) {
    case 0x00: //RLC B
      boolean = (state->b > 127); state->b = seperateBytes(state->b * 2).second;
      if (boolean) state->b++; setFlags(state->b, 0, 2, 0, 0, boolean); state->cycles += 8; break;
    case 0x01: //RLC C
      boolean = (state->c > 127); state->c = seperateBytes(state->c * 2).second;
      if (boolean) state->c++; setFlags(state->c, 0, 2, 0, 0, boolean); state->cycles += 8; break;
    case 0x02: //RLC D
      boolean = (state->d > 127); state->d = seperateBytes(state->d * 2).second;
      if (boolean) state->d++; setFlags(state->d, 0, 2, 0, 0, boolean); state->cycles += 8; break;
    case 0x03: //RLC E
      boolean = (state->e > 127); state->e = seperateBytes(state->e * 2).second;
      if (boolean) state->e++; setFlags(state->e, 0, 2, 0, 0, boolean); state->cycles += 8; break;
    case 0x04: //RLC H
      boolean = (state->h > 127); state->h = seperateBytes(state->h * 2).second;
      if (boolean) state->h++; setFlags(state->h, 0, 2, 0, 0, boolean); state->cycles += 8; break;
    case 0x05: //RLC L
      boolean = (state->l > 127); state->l = seperateBytes(state->l * 2).second;
      if (boolean) state->l++; setFlags(state->l, 0, 2, 0, 0, boolean); state->cycles += 8; break;
    case 0x06: //RLC (HL)
      addr = (state->h * 256) + state->l; boolean = (state->memory[extendAddr(addr)] > 127);
      if (!isRom(addr)) state->memory[extendAddr(addr)] = seperateBytes(state->memory[extendAddr(addr)] * 2).second;
      if (boolean) state->memory[extendAddr(addr)]++; setFlags(state->memory[extendAddr(addr)], 0, 2, 0, 0, boolean);
      state->cycles += 16; break;
    case 0x07: //RLC A
      boolean = (state->a > 127); state->a = seperateBytes(state->a * 2).second;
      if (boolean) state->a++; setFlags(state->a, 0, 2, 0, 0, boolean); state->cycles += 8; break;

    case 0x08: //RRC B
      boolean = (state->b % 2 != 0);  state->b = floor(state->b / 2); if (boolean) state->b += 128;
      setFlags(state->b, 0, 2, 0, 0, boolean); state->cycles += 8; break;
    case 0x09: //RRC C
      boolean = (state->c % 2 != 0);  state->c = floor(state->c / 2); if (boolean) state->c += 128;
      setFlags(state->c, 0, 2, 0, 0, boolean); state->cycles += 8; break;
    case 0x0a: //RRC D
      boolean = (state->d % 2 != 0);  state->d = floor(state->d / 2); if (boolean) state->d += 128;
      setFlags(state->d, 0, 2, 0, 0, boolean); state->cycles += 8; break;
    case 0x0b: //RRC E
      boolean = (state->e % 2 != 0);  state->e = floor(state->e / 2); if (boolean) state->e += 128;
      setFlags(state->e, 0, 2, 0, 0, boolean); state->cycles += 8; break;
    case 0x0c: //RRC H
      boolean = (state->h % 2 != 0);  state->h = floor(state->h / 2); if (boolean) state->h += 128;
      setFlags(state->h, 0, 2, 0, 0, boolean); state->cycles += 8; break;
    case 0x0d: //RRC L
      boolean = (state->l % 2 != 0);  state->l = floor(state->l / 2); if (boolean) state->l += 128;
      setFlags(state->l, 0, 2, 0, 0, boolean); state->cycles += 8; break;
    case 0x0e: //RRC (HL)
      addr = (state->h * 256) + state->l; boolean = (state->memory[extendAddr(addr)] % 2 != 0);
      if (!isRom(addr)) state->memory[extendAddr(addr)] = floor(state->memory[extendAddr(addr)] / 2);
      if (boolean) state->memory[extendAddr(addr)] += 128;
      setFlags(state->memory[extendAddr(addr)], 0, 2, 0, 0, boolean); state->cycles += 16; break;
    case 0x0f: //RRC A
      boolean = (state->a % 2 != 0);  state->a = floor(state->a / 2); if (boolean) state->a += 128;
      setFlags(state->a, 0, 2, 0, 0, boolean); state->cycles += 8; break;

    case 0x10:
      boolean = state->flags.cy; setFlags(state->b, 0, 6, 0, 0, 3); value16 = state->b * 2;
      state->b = seperateBytes(value16).second; if (boolean) state->b++; state->cycles += 8; break;
    case 0x11:
      boolean = state->flags.cy; setFlags(state->c, 0, 6, 0, 0, 3); value16 = state->c * 2;
      state->c = seperateBytes(value16).second; if (boolean) state->c++; state->cycles += 8; break;
    case 0x12:
      boolean = state->flags.cy; setFlags(state->d, 0, 6, 0, 0, 3); value16 = state->d * 2;
      state->d = seperateBytes(value16).second; if (boolean) state->d++; state->cycles += 8; break;
    case 0x13:
      boolean = state->flags.cy; setFlags(state->e, 0, 6, 0, 0, 3); value16 = state->e * 2;
      state->e = seperateBytes(value16).second; if (boolean) state->e++; state->cycles += 8; break;
    case 0x14:
      boolean = state->flags.cy; setFlags(state->h, 0, 6, 0, 0, 3); value16 = state->h * 2;
      state->h = seperateBytes(value16).second; if (boolean) state->h++; state->cycles += 8; break;
    case 0x15:
      boolean = state->flags.cy; setFlags(state->l, 0, 6, 0, 0, 3); value16 = state->l * 2;
      state->l = seperateBytes(value16).second; if (boolean) state->l++; state->cycles += 8; break;
    case 0x16:
      addr = (state->h * 256) + state->l;boolean = state->flags.cy;
      setFlags(state->memory[extendAddr(addr)], 0, 6, 0, 0, 3); value16 = state->memory[extendAddr(addr)] * 2;
      if (!isRom(addr)) state->memory[extendAddr(addr)] = seperateBytes(value16).second;
      if (boolean) state->memory[extendAddr(addr)]++; state->cycles += 16; break;
    case 0x17:
      boolean = state->flags.cy; setFlags(state->a, 0, 6, 0, 0, 3); value16 = state->a * 2;
      state->a = seperateBytes(value16).second; if (boolean) state->a++; state->cycles += 8; break;

    case 0x18: //RR B
      boolean = state->flags.cy; setFlags(state->b, 0, 8, 0, 0, 6); state->b = floor(state->b / 2);
      if (boolean) state->b += 128; state->cycles += 8; break;
    case 0x19: //RR C
      boolean = state->flags.cy; setFlags(state->c, 0, 8, 0, 0, 6); state->c = floor(state->c / 2);
      if (boolean) state->c += 128; state->cycles += 8; break;
    case 0x1a: //RR D
      boolean = state->flags.cy; setFlags(state->d, 0, 8, 0, 0, 6); state->d = floor(state->d / 2);
      if (boolean) state->d += 128; state->cycles += 8; break;
    case 0x1b: //RR E
      boolean = state->flags.cy; setFlags(state->e, 0, 8, 0, 0, 6); state->e = floor(state->e / 2);
      if (boolean) state->e += 128; state->cycles += 8; break;
    case 0x1c: //RR H
      boolean = state->flags.cy; setFlags(state->h, 0, 8, 0, 0, 6); state->h = floor(state->h / 2);
      if (boolean) state->h += 128; state->cycles += 8; break;
    case 0x1d: //RR L
      boolean = state->flags.cy; setFlags(state->l, 0, 8, 0, 0, 6); state->l = floor(state->l / 2);
      if (boolean) state->l += 128; state->cycles += 8; break;
    case 0x1e: //RR A
      addr = (state->h * 256) + state->l;
      boolean = state->flags.cy; setFlags(state->memory[extendAddr(addr)], 0, 8, 0, 0, 6);
      if (!isRom(addr)) state->memory[extendAddr(addr)] = floor(state->memory[extendAddr(addr)] / 2);
      if (boolean) state->memory[extendAddr(addr)] += 128; state->cycles += 16; break;
    case 0x1f: //RR A
      boolean = state->flags.cy; setFlags(state->a, 0, 8, 0, 0, 6); state->a = floor(state->a / 2);
      if (boolean) state->a += 128; state->cycles += 8; break;

    case 0x20:
      boolean = state->b > 127; state->b = seperateBytes(state->b * 2).second;
      setFlags(state->b, 0, 2, 0, 0, boolean); state->cycles += 8; break;
    case 0x21:
      boolean = state->c > 127; state->c = seperateBytes(state->c * 2).second;
      setFlags(state->c, 0, 2, 0, 0, boolean); state->cycles += 8; break;
    case 0x22:
      boolean = state->d > 127; state->d = seperateBytes(state->d * 2).second;
      setFlags(state->d, 0, 2, 0, 0, boolean); state->cycles += 8; break;
    case 0x23:
      boolean = state->e > 127; state->e = seperateBytes(state->e * 2).second;
      setFlags(state->e, 0, 2, 0, 0, boolean); state->cycles += 8; break;
    case 0x24:
      boolean = state->h > 127; state->h = seperateBytes(state->h * 2).second;
      setFlags(state->h, 0, 2, 0, 0, boolean); state->cycles += 8; break;
    case 0x25:
      boolean = state->l > 127; state->l = seperateBytes(state->l * 2).second;
      setFlags(state->l, 0, 2, 0, 0, boolean); state->cycles += 8; break;
    case 0x26:
      addr = (state->h * 256) + state->l; boolean = state->memory[extendAddr(addr)] > 127;
      if (!isRom(addr)) state->memory[extendAddr(addr)] = seperateBytes(state->memory[extendAddr(addr)] * 2).second;
      setFlags(state->memory[extendAddr(addr)], 0, 2, 0, 0, boolean); state->cycles += 8; break;
    case 0x27:
      boolean = state->a > 127; state->a = seperateBytes(state->a * 2).second;
      setFlags(state->a, 0, 2, 0, 0, boolean); state->cycles += 8; break;

    case 0x28: //SRA B
      boolean = state->b > 127; setFlags(state->b, 0, 9, 0, 0, 6); state->b = floor(state->b / 2);
      if (boolean) state->b += 128; state->cycles += 8; break;
    case 0x29: //SRA C
      boolean = state->c > 127; setFlags(state->c, 0, 9, 0, 0, 6); state->c = floor(state->c / 2);
      if (boolean) state->c += 128; state->cycles += 8; break;
    case 0x2a: //SRA D
      boolean = state->d > 127; setFlags(state->d, 0, 9, 0, 0, 6); state->d = floor(state->d / 2);
      if (boolean) state->d += 128; state->cycles += 8; break;
    case 0x2b: //SRA E
      boolean = state->e > 127; setFlags(state->e, 0, 9, 0, 0, 6); state->e = floor(state->e / 2);
      if (boolean) state->e += 128; state->cycles += 8; break;
    case 0x2c: //SRA H
      boolean = state->h > 127; setFlags(state->h, 0, 9, 0, 0, 6); state->h = floor(state->h / 2);
      if (boolean) state->h += 128; state->cycles += 8; break;
    case 0x2d: //SRA L
      boolean = state->l > 127; setFlags(state->l, 0, 9, 0, 0, 6); state->l = floor(state->l / 2);
      if (boolean) state->l += 128; state->cycles += 8; break;
    case 0x2e: //SRA (HL)
      addr = (state->h * 256) + state->l;
      boolean = state->memory[extendAddr(addr)] > 127; setFlags(state->memory[extendAddr(addr)], 0, 9, 0, 0, 6);
      if (!isRom(addr)) state->memory[extendAddr(addr)] = floor(state->memory[extendAddr(addr)] / 2);
      if (boolean) state->memory[extendAddr(addr)] += 128; state->cycles += 16; break;
    case 0x2f: //SRA A
      boolean = state->a > 127; setFlags(state->a, 0, 9, 0, 0, 6); state->a = floor(state->a / 2);
      if (boolean) state->a += 128; state->cycles += 8; break;

    case 0x30:
      setFlags(state->b, 0, 2, 0, 0, 0); value = state->b & 0x0F; value2 = (state->b & 0xF0) >> 4;
      state->b = (value * 16) + value2; state->cycles += 8; break;
    case 0x31:
      setFlags(state->c, 0, 2, 0, 0, 0); value = state->c & 0x0F; value2 = (state->c & 0xF0) >> 4;
      state->c = (value * 16) + value2; state->cycles += 8; break;
    case 0x32:
      setFlags(state->d, 0, 2, 0, 0, 0); value = state->d & 0x0F; value2 = (state->d & 0xF0) >> 4;
      state->d = (value * 16) + value2; state->cycles += 8; break;
    case 0x33:
      setFlags(state->e, 0, 2, 0, 0, 0); value = state->e & 0x0F; value2 = (state->e & 0xF0) >> 4;
      state->e = (value * 16) + value2; state->cycles += 8; break;
    case 0x34:
      setFlags(state->h, 0, 2, 0, 0, 0); value = state->h & 0x0F; value2 = (state->h & 0xF0) >> 4;
      state->h = (value * 16) + value2; state->cycles += 8; break;
    case 0x35:
      setFlags(state->l, 0, 2, 0, 0, 0); value = state->l & 0x0F; value2 = (state->l & 0xF0) >> 4;
      state->l = (value * 16) + value2; state->cycles += 8; break;
    case 0x36:
      addr = (state->h * 256) + state->l; setFlags(state->memory[extendAddr(addr)], 0, 2, 0, 0, 0);
      value = state->memory[extendAddr(addr)] & 0x0F; value2 = (state->memory[extendAddr(addr)] & 0xF0) >> 4;
      if (!isRom(addr)) state->memory[extendAddr(addr)] = (value * 16) + value2;
      state->cycles += 16; break;
    case 0x37:
      setFlags(state->a, 0, 2, 0, 0, 0); value = state->a & 0x0F; value2 = (state->a & 0xF0) >> 4;
      state->a = (value * 16) + value2; state->cycles += 8; break;

    case 0x38:
      setFlags(state->b, 254, 5, 0, 0, 6); state->b = floor(state->b / 2); state->cycles += 8; break;
    case 0x39:
      setFlags(state->c, 254, 5, 0, 0, 6); state->c = floor(state->c / 2); state->cycles += 8; break;
    case 0x3a:
      setFlags(state->d, 254, 5, 0, 0, 6); state->d = floor(state->d / 2); state->cycles += 8; break;
    case 0x3b:
      setFlags(state->e, 254, 5, 0, 0, 6); state->e = floor(state->e / 2); state->cycles += 8; break;
    case 0x3c:
      setFlags(state->h, 254, 5, 0, 0, 6); state->h = floor(state->h / 2); state->cycles += 8; break;
    case 0x3d:
      setFlags(state->l, 254, 5, 0, 0, 6); state->l = floor(state->l / 2); state->cycles += 8; break;
    case 0x3e:
      addr = (state->h * 256) + state->l; setFlags(state->memory[extendAddr(addr)], 254, 5, 0, 0, 6);
      if (!isRom(addr)) state->memory[extendAddr(addr)] = floor(state->memory[extendAddr(addr)] / 2);
      state->cycles += 16; break;
    case 0x3f:
      setFlags(state->a, 254, 5, 0, 0, 6); state->a = floor(state->a / 2); state->cycles += 8; break;

    case 0x40:  ///bit 0 of register b
      setFlags(state->b, (1 << 0), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x41:  ///bit 0 of register c
      setFlags(state->c, (1 << 0), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x42:  ///bit 0 of register d
      setFlags(state->d, (1 << 0), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x43:  ///bit 0 of register e
      setFlags(state->e, (1 << 0), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x44:  ///bit 0 of register h
      setFlags(state->h, (1 << 0), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x45:  ///bit 0 of register l
      setFlags(state->l, (1 << 0), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x46:  ///bit 0 of (hl)
      addr = (state->h * 256) + state->l;
      setFlags(state->memory[extendAddr(addr)], (1 << 0), 5, 0, 1, -1); state->cycles += 16; break;
    case 0x47:  ///bit 0 of register a
      setFlags(state->a, (1 << 0), 5, 0, 1, -1); state->cycles += 8; break;

    case 0x48:  ///bit 1 of register b
      setFlags(state->b, (1 << 1), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x49:  ///bit 1 of register c
      setFlags(state->c, (1 << 1), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x4a:  ///bit 1 of register d
      setFlags(state->d, (1 << 1), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x4b:  ///bit 1 of register e
      setFlags(state->e, (1 << 1), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x4c:  ///bit 1 of register h
      setFlags(state->h, (1 << 1), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x4d:  ///bit 1 of register l
      setFlags(state->l, (1 << 1), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x4e:  ///bit 1 of (hl)
      addr = (state->h * 256) + state->l;
      setFlags(state->memory[extendAddr(addr)], (1 << 1), 5, 0, 1, -1); state->cycles += 16; break;
    case 0x4f:  ///bit 1 of register a
      setFlags(state->a, (1 << 1), 5, 0, 1, -1); state->cycles += 8; break;

    case 0x50:  ///bit 2 of register b
      setFlags(state->b, (1 << 2), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x51:  ///bit 2 of register c
      setFlags(state->c, (1 << 2), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x52:  ///bit 2 of register d
      setFlags(state->d, (1 << 2), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x53:  ///bit 2 of register e
      setFlags(state->e, (1 << 2), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x54:  ///bit 2 of register h
      setFlags(state->h, (1 << 2), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x55:  ///bit 2 of register l
      setFlags(state->l, (1 << 2), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x56:  ///bit 2 of (hl)
      addr = (state->h * 256) + state->l;
      setFlags(state->memory[extendAddr(addr)], (1 << 2), 5, 0, 1, -1); state->cycles += 16; break;
    case 0x57:  ///bit 2 of register a
      setFlags(state->a, (1 << 2), 5, 0, 1, -1); state->cycles += 8; break;

    case 0x58:  ///bit 3 of register b
      setFlags(state->b, (1 << 3), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x59:  ///bit 3 of register c
      setFlags(state->c, (1 << 3), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x5a:  ///bit 3 of register d
      setFlags(state->d, (1 << 3), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x5b:  ///bit 3 of register e
      setFlags(state->e, (1 << 3), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x5c:  ///bit 3 of register h
      setFlags(state->h, (1 << 3), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x5d:  ///bit 3 of register l
      setFlags(state->l, (1 << 3), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x5e:  ///bit 3 of (hl)
      addr = (state->h * 256) + state->l;
      setFlags(state->memory[extendAddr(addr)], (1 << 3), 5, 0, 1, -1); state->cycles += 16; break;
    case 0x5f:  ///bit 3 of register a
      setFlags(state->a, (1 << 3), 5, 0, 1, -1); state->cycles += 8; break;

    case 0x60:  ///bit 4 of register b
      setFlags(state->b, (1 << 4), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x61:  ///bit 4 of register c
      setFlags(state->c, (1 << 4), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x62:  ///bit 4 of register d
      setFlags(state->d, (1 << 4), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x63:  ///bit 4 of register e
      setFlags(state->e, (1 << 4), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x64:  ///bit 4 of register h
      setFlags(state->h, (1 << 4), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x65:  ///bit 4 of register l
      setFlags(state->l, (1 << 4), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x66:  ///bit 4 of (hl)
      addr = (state->h * 256) + state->l;
      setFlags(state->memory[extendAddr(addr)], (1 << 4), 5, 0, 1, -1); state->cycles += 16; break;
    case 0x67:  ///bit 4 of register a
      setFlags(state->a, (1 << 4), 5, 0, 1, -1); state->cycles += 8; break;

    case 0x68:  ///bit 5 of register b
      setFlags(state->b, (1 << 5), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x69:  ///bit 5 of register c
      setFlags(state->c, (1 << 5), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x6a:  ///bit 5 of register d
      setFlags(state->d, (1 << 5), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x6b:  ///bit 5 of register e
      setFlags(state->e, (1 << 5), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x6c:  ///bit 5 of register h
      setFlags(state->h, (1 << 5), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x6d:  ///bit 5 of register l
      setFlags(state->l, (1 << 5), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x6e:  ///bit 5 of (hl)
      addr = (state->h * 256) + state->l;
      setFlags(state->memory[extendAddr(addr)], (1 << 5), 5, 0, 1, -1); state->cycles += 16; break;
    case 0x6f:  ///bit 5 of register a
      setFlags(state->a, (1 << 5), 5, 0, 1, -1); state->cycles += 8; break;

    case 0x70:  ///bit 6 of register b
      setFlags(state->b, (1 << 6), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x71:  ///bit 6 of register c
      setFlags(state->c, (1 << 6), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x72:  ///bit 6 of register d
      setFlags(state->d, (1 << 6), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x73:  ///bit 6 of register e
      setFlags(state->e, (1 << 6), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x74:  ///bit 6 of register h
      setFlags(state->h, (1 << 6), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x75:  ///bit 6 of register l
      setFlags(state->l, (1 << 6), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x76:  ///bit 6 of (hl)
      addr = (state->h * 256) + state->l;
      setFlags(state->memory[extendAddr(addr)], (1 << 6), 5, 0, 1, -1); state->cycles += 16; break;
    case 0x77:  ///bit 6 of register a
      setFlags(state->a, (1 << 6), 5, 0, 1, -1); state->cycles += 8; break;

    case 0x78:  ///bit 7 of register b
      setFlags(state->b, (1 << 7), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x79:  ///bit 7 of register c
      setFlags(state->c, (1 << 7), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x7a:  ///bit 7 of register d
      setFlags(state->d, (1 << 7), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x7b:  ///bit 7 of register e
      setFlags(state->e, (1 << 7), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x7c:  ///bit 7 of register h
      setFlags(state->h, (1 << 7), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x7d:  ///bit 7 of register l
      setFlags(state->l, (1 << 7), 5, 0, 1, -1); state->cycles += 8; break;
    case 0x7e:  ///bit 7 of (hl)
      addr = (state->h * 256) + state->l;
      setFlags(state->memory[extendAddr(addr)], (1 << 7), 5, 0, 1, -1); state->cycles += 16; break;
    case 0x7f:  ///bit 7 of register a
      setFlags(state->a, (1 << 7), 5, 0, 1, -1); state->cycles += 8; break;

    case 0x80: //RESET bit 0 of register B
      state->b = state->b & ~(1 << 0); state->cycles += 8; break;
    case 0x81: //RESET bit 0 of register C
      state->c = state->c & ~(1 << 0); state->cycles += 8; break;
    case 0x82: //RESET bit 0 of register D
      state->d = state->d & ~(1 << 0); state->cycles += 8; break;
    case 0x83: //RESET bit 0 of register E
      state->e = state->e & ~(1 << 0); state->cycles += 8; break;
    case 0x84: //RESET bit 0 of register H
      state->h = state->h & ~(1 << 0); state->cycles += 8; break;
    case 0x85: //RESET bit 0 of register L
      state->l = state->l & ~(1 << 0); state->cycles += 8; break;
    case 0x86: //RESET bit 0 of (HL)
      addr = (state->h * 256) + state->l;
      if (!isRom(addr)) state->memory[extendAddr(addr)] = state->memory[extendAddr(addr)] & ~(1 << 0);
      state->cycles += 16; break;
    case 0x87: //RESET bit 0 of register A
      state->a = state->a & ~(1 << 0); state->cycles += 8; break;

    case 0x88: //RESET bit 1 of register B
      state->b = state->b & ~(1 << 1); state->cycles += 8; break;
    case 0x89: //RESET bit 1 of register C
      state->c = state->c & ~(1 << 1); state->cycles += 8; break;
    case 0x8a: //RESET bit 1 of register D
      state->d = state->d & ~(1 << 1); state->cycles += 8; break;
    case 0x8b: //RESET bit 1 of register E
      state->e = state->e & ~(1 << 1); state->cycles += 8; break;
    case 0x8c: //RESET bit 1 of register H
      state->h = state->h & ~(1 << 1); state->cycles += 8; break;
    case 0x8d: //RESET bit 1 of register L
      state->l = state->l & ~(1 << 1); state->cycles += 8; break;
    case 0x8e: //RESET bit 1 of (HL)
      addr = (state->h * 256) + state->l;
      if (!isRom(addr)) state->memory[extendAddr(addr)] = state->memory[extendAddr(addr)] & ~(1 << 1);
      state->cycles += 16; break;
    case 0x8f: //RESET bit 1 of register A
      state->a = state->a & ~(1 << 1); state->cycles += 8; break;

    case 0x90: //RESET bit 2 of register B
      state->b = state->b & ~(1 << 2); state->cycles += 8; break;
    case 0x91: //RESET bit 2 of register C
      state->c = state->c & ~(1 << 2); state->cycles += 8; break;
    case 0x92: //RESET bit 2 of register D
      state->d = state->d & ~(1 << 2); state->cycles += 8; break;
    case 0x93: //RESET bit 2 of register E
      state->e = state->e & ~(1 << 2); state->cycles += 8; break;
    case 0x94: //RESET bit 2 of register H
      state->h = state->h & ~(1 << 2); state->cycles += 8; break;
    case 0x95: //RESET bit 2 of register L
      state->l = state->l & ~(1 << 2); state->cycles += 8; break;
    case 0x96: //RESET bit 2 of (HL)
      addr = (state->h * 256) + state->l;
      if (!isRom(addr)) state->memory[extendAddr(addr)] = state->memory[extendAddr(addr)] & ~(1 << 2);
      state->cycles += 16; break;
    case 0x97: //RESET bit 2 of register A
      state->a = state->a & ~(1 << 2); state->cycles += 8; break;

    case 0x98: //RESET bit 3 of register B
      state->b = state->b & ~(1 << 3); state->cycles += 8; break;
    case 0x99: //RESET bit 3 of register C
      state->c = state->c & ~(1 << 3); state->cycles += 8; break;
    case 0x9a: //RESET bit 3 of register D
      state->d = state->d & ~(1 << 3); state->cycles += 8; break;
    case 0x9b: //RESET bit 3 of register E
      state->e = state->e & ~(1 << 3); state->cycles += 8; break;
    case 0x9c: //RESET bit 3 of register H
      state->h = state->h & ~(1 << 3); state->cycles += 8; break;
    case 0x9d: //RESET bit 3 of register L
      state->l = state->l & ~(1 << 3); state->cycles += 8; break;
    case 0x9e: //RESET bit 3 of (HL)
      addr = (state->h * 256) + state->l;
      if (!isRom(addr)) state->memory[extendAddr(addr)] = state->memory[extendAddr(addr)] & ~(1 << 3);
      state->cycles += 16; break;
    case 0x9f: //RESET bit 3 of register A
      state->a = state->a & ~(1 << 3); state->cycles += 8; break;

    case 0xa0: //RESET bit 4 of register B
      state->b = state->b & ~(1 << 4); state->cycles += 8; break;
    case 0xa1: //RESET bit 4 of register C
      state->c = state->c & ~(1 << 4); state->cycles += 8; break;
    case 0xa2: //RESET bit 4 of register D
      state->d = state->d & ~(1 << 4); state->cycles += 8; break;
    case 0xa3: //RESET bit 4 of register E
      state->e = state->e & ~(1 << 4); state->cycles += 8; break;
    case 0xa4: //RESET bit 4 of register H
      state->h = state->h & ~(1 << 4); state->cycles += 8; break;
    case 0xa5: //RESET bit 4 of register L
      state->l = state->l & ~(1 << 4); state->cycles += 8; break;
    case 0xa6: //RESET bit 4 of (HL)
      addr = (state->h * 256) + state->l;
      if (!isRom(addr)) state->memory[extendAddr(addr)] = state->memory[extendAddr(addr)] & ~(1 << 4);
      state->cycles += 16; break;
    case 0xa7: //RESET bit 4 of register A
      state->a = state->a & ~(1 << 4); state->cycles += 8; break;


    case 0xa8: //RESET bit 5 of register B
      state->b = state->b & ~(1 << 5); state->cycles += 8; break;
    case 0xa9: //RESET bit 5 of register C
      state->c = state->c & ~(1 << 5); state->cycles += 8; break;
    case 0xaa: //RESET bit 5 of register D
      state->d = state->d & ~(1 << 5); state->cycles += 8; break;
    case 0xab: //RESET bit 5 of register E
      state->e = state->e & ~(1 << 5); state->cycles += 8; break;
    case 0xac: //RESET bit 5 of register H
      state->h = state->h & ~(1 << 5); state->cycles += 8; break;
    case 0xad: //RESET bit 5 of register L
      state->l = state->l & ~(1 << 5); state->cycles += 8; break;
    case 0xae: //RESET bit 5 of (HL)
      addr = (state->h * 256) + state->l;
      if (!isRom(addr)) state->memory[extendAddr(addr)] = state->memory[extendAddr(addr)] & ~(1 << 5);
      state->cycles += 16; break;
    case 0xaf: //RESET bit 5 of register A
      state->a = state->a & ~(1 << 5); state->cycles += 8; break;

    case 0xb0: //RESET bit 6 of register B
      state->b = state->b & ~(1 << 6); state->cycles += 8; break;
    case 0xb1: //RESET bit 6 of register C
      state->c = state->c & ~(1 << 6); state->cycles += 8; break;
    case 0xb2: //RESET bit 6 of register D
      state->d = state->d & ~(1 << 6); state->cycles += 8; break;
    case 0xb3: //RESET bit 6 of register E
      state->e = state->e & ~(1 << 6); state->cycles += 8; break;
    case 0xb4: //RESET bit 6 of register H
      state->h = state->h & ~(1 << 6); state->cycles += 8; break;
    case 0xb5: //RESET bit 6 of register L
      state->l = state->l & ~(1 << 6); state->cycles += 8; break;
    case 0xb6: //RESET bit 6 of (HL)
      addr = (state->h * 256) + state->l;
      if (!isRom(addr)) state->memory[extendAddr(addr)] = state->memory[extendAddr(addr)] & ~(1 << 6);
      state->cycles += 16; break;
    case 0xb7: //RESET bit 6 of register A
      state->a = state->a & ~(1 << 6); state->cycles += 8; break;

    case 0xb8: //RESET bit 7 of register B
      state->b = state->b & ~(1 << 7); state->cycles += 8; break;
    case 0xb9: //RESET bit 7 of register C
      state->c = state->c & ~(1 << 7); state->cycles += 8; break;
    case 0xba: //RESET bit 7 of register D
      state->d = state->d & ~(1 << 7); state->cycles += 8; break;
    case 0xbb: //RESET bit 7 of register E
      state->e = state->e & ~(1 << 7); state->cycles += 8; break;
    case 0xbc: //RESET bit 7 of register H
      state->h = state->h & ~(1 << 7); state->cycles += 8; break;
    case 0xbd: //RESET bit 7 of register L
      state->l = state->l & ~(1 << 7); state->cycles += 8; break;
    case 0xbe: //RESET bit 7 of (HL)
      addr = (state->h * 256) + state->l;
      if (!isRom(addr)) state->memory[extendAddr(addr)] = state->memory[extendAddr(addr)] & ~(1 << 7);
      state->cycles += 16; break;
    case 0xbf: //RESET bit 7 of A
      state->a = state->a & ~(1 << 7); state->cycles += 16; break;

    case 0xc0: // SET bit 0 of register B
      state->b = state->b | (1 << 0); state->cycles += 8; break;
    case 0xc1: // SET bit 0 of register C
      state->c = state->c | (1 << 0); state->cycles += 8; break;
    case 0xc2: // SET bit 0 of register D
      state->d = state->d | (1 << 0); state->cycles += 8; break;
    case 0xc3: // SET bit 0 of register E
      state->e = state->e | (1 << 0); state->cycles += 8; break;
    case 0xc4: // SET bit 0 of register H
      state->h = state->h | (1 << 0); state->cycles += 8; break;
    case 0xc5: // SET bit 0 of register L
      state->l = state->l | (1 << 0); state->cycles += 8; break;
    case 0xc6: // SET bit 0 of register (HL)
      addr = (state->h * 256) + state->l;
      if (!isRom(addr)) state->memory[extendAddr(addr)] = state->memory[extendAddr(addr)] | (1 << 0);
      state->cycles += 16; break;
    case 0xc7: // SET bit 0 of register A
      state->a = state->a | (1 << 0); state->cycles += 8; break;

    case 0xc8: // SET bit 1 of register B
      state->b = state->b | (1 << 1); state->cycles += 8; break;
    case 0xc9: // SET bit 1 of register C
      state->c = state->c | (1 << 1); state->cycles += 8; break;
    case 0xca: // SET bit 1 of register D
      state->d = state->d | (1 << 1); state->cycles += 8; break;
    case 0xcb: // SET bit 1 of register E
      state->e = state->e | (1 << 1); state->cycles += 8; break;
    case 0xcc: // SET bit 1 of register H
      state->h = state->h | (1 << 1); state->cycles += 8; break;
    case 0xcd: // SET bit 1 of register L
      state->l = state->l | (1 << 1); state->cycles += 8; break;
    case 0xce: // SET bit 1 of register (HL)
      addr = (state->h * 256) + state->l;
      if (!isRom(addr)) state->memory[extendAddr(addr)] = state->memory[extendAddr(addr)] | (1 << 1);
      state->cycles += 16; break;
    case 0xcf: // SET bit 1 of register A
      state->a = state->a | (1 << 1); state->cycles += 8; break;

    case 0xd0: // SET bit 2 of register B
      state->b = state->b | (1 << 2); state->cycles += 8; break;
    case 0xd1: // SET bit 2 of register C
      state->c = state->c | (1 << 2); state->cycles += 8; break;
    case 0xd2: // SET bit 2 of register D
      state->d = state->d | (1 << 2); state->cycles += 8; break;
    case 0xd3: // SET bit 2 of register E
      state->e = state->e | (1 << 2); state->cycles += 8; break;
    case 0xd4: // SET bit 2 of register H
      state->h = state->h | (1 << 2); state->cycles += 8; break;
    case 0xd5: // SET bit 2 of register L
      state->l = state->l | (1 << 2); state->cycles += 8; break;
    case 0xd6: // SET bit 2 of register (HL)
      addr = (state->h * 256) + state->l;
      if (!isRom(addr)) state->memory[extendAddr(addr)] = state->memory[extendAddr(addr)] | (1 << 2);
      state->cycles += 16; break;
    case 0xd7: // SET bit 2 of register A
      state->a = state->a | (1 << 2); state->cycles += 8; break;

    case 0xd8: // SET bit 3 of register B
      state->b = state->b | (1 << 3); state->cycles += 8; break;
    case 0xd9: // SET bit 3 of register C
      state->c = state->c | (1 << 3); state->cycles += 8; break;
    case 0xda: // SET bit 3 of register D
      state->d = state->d | (1 << 3); state->cycles += 8; break;
    case 0xdb: // SET bit 3 of register E
      state->e = state->e | (1 << 3); state->cycles += 8; break;
    case 0xdc: // SET bit 3 of register H
      state->h = state->h | (1 << 3); state->cycles += 8; break;
    case 0xdd: // SET bit 3 of register L
      state->l = state->l | (1 << 3); state->cycles += 8; break;
    case 0xde: // SET bit 3 of register (HL)
      addr = (state->h * 256) + state->l;
      if (!isRom(addr)) state->memory[extendAddr(addr)] = state->memory[extendAddr(addr)] | (1 << 3);
      state->cycles += 16; break;
    case 0xdf: // SET bit 3 of register A
      state->a = state->a | (1 << 3); state->cycles += 8; break;

    case 0xe0: // SET bit 4 of register B
      state->b = state->b | (1 << 4); state->cycles += 8; break;
    case 0xe1: // SET bit 4 of register C
      state->c = state->c | (1 << 4); state->cycles += 8; break;
    case 0xe2: // SET bit 4 of register D
      state->d = state->d | (1 << 4); state->cycles += 8; break;
    case 0xe3: // SET bit 4 of register E
      state->e = state->e | (1 << 4); state->cycles += 8; break;
    case 0xe4: // SET bit 4 of register H
      state->h = state->h | (1 << 4); state->cycles += 8; break;
    case 0xe5: // SET bit 4 of register L
      state->l = state->l | (1 << 4); state->cycles += 8; break;
    case 0xe6: // SET bit 4 of register (HL)
      addr = (state->h * 256) + state->l;
      if (!isRom(addr)) state->memory[extendAddr(addr)] = state->memory[extendAddr(addr)] | (1 << 4);
      state->cycles += 16; break;
    case 0xe7: // SET bit 4 of register A
      state->a = state->a | (1 << 4); state->cycles += 8; break;

    case 0xe8: // SET bit 5 of register B
      state->b = state->b | (1 << 5); state->cycles += 8; break;
    case 0xe9: // SET bit 5 of register C
      state->c = state->c | (1 << 5); state->cycles += 8; break;
    case 0xea: // SET bit 5 of register D
      state->d = state->d | (1 << 5); state->cycles += 8; break;
    case 0xeb: // SET bit 5 of register E
      state->e = state->e | (1 << 5); state->cycles += 8; break;
    case 0xec: // SET bit 5 of register H
      state->h = state->h | (1 << 5); state->cycles += 8; break;
    case 0xed: // SET bit 5 of register L
      state->l = state->l | (1 << 5); state->cycles += 8; break;
    case 0xee: // SET bit 5 of register (HL)
      addr = (state->h * 256) + state->l;
      if (!isRom(addr)) state->memory[extendAddr(addr)] = state->memory[extendAddr(addr)] | (1 << 5);
      state->cycles += 16; break;
    case 0xef: // SET bit 5 of register A
      state->a = state->a | (1 << 5); state->cycles += 8; break;

    case 0xf0: // SET bit 6 of register B
      state->b = state->b | (1 << 6); state->cycles += 8; break;
    case 0xf1: // SET bit 6 of register C
      state->c = state->c | (1 << 6); state->cycles += 8; break;
    case 0xf2: // SET bit 6 of register D
      state->d = state->d | (1 << 6); state->cycles += 8; break;
    case 0xf3: // SET bit 6 of register E
      state->e = state->e | (1 << 6); state->cycles += 8; break;
    case 0xf4: // SET bit 6 of register H
      state->h = state->h | (1 << 6); state->cycles += 8; break;
    case 0xf5: // SET bit 6 of register L
      state->l = state->l | (1 << 6); state->cycles += 8; break;
    case 0xf6: // SET bit 6 of register (HL)
      addr = (state->h * 256) + state->l;
      if (!isRom(addr)) state->memory[extendAddr(addr)] = state->memory[extendAddr(addr)] | (1 << 6);
      state->cycles += 16; break;
    case 0xf7: // SET bit 6 of register A
      state->a = state->a | (1 << 6); state->cycles += 8; break;

    case 0xf8: // SET bit 7 of register B
      state->b = state->b | (1 << 7); state->cycles += 8; break;
    case 0xf9: // SET bit 7 of register C
      state->c = state->c | (1 << 7); state->cycles += 8; break;
    case 0xfa: // SET bit 7 of register D
      state->d = state->d | (1 << 7); state->cycles += 8; break;
    case 0xfb: // SET bit 7 of register E
      state->e = state->e | (1 << 7); state->cycles += 8; break;
    case 0xfc: // SET bit 7 of register H
      state->h = state->h | (1 << 7); state->cycles += 8; break;
    case 0xfd: // SET bit 7 of register L
      state->l = state->l | (1 << 7); state->cycles += 8; break;
    case 0xfe: // SET bit 7 of register (HL)
      addr = (state->h * 256) + state->l;
      if (!isRom(addr)) state->memory[extendAddr(addr)] = state->memory[extendAddr(addr)] | (1 << 7);
      state->cycles += 16; break;
    case 0xff: // SET bit 7 of register A
      state->a = state->a | (1 << 7); state->cycles += 8; break;

    default:
      cout << "Unimplemented CB Opcode" << endl;
      return unimplementedOpcode(cb_opcode);
  }

  return true;  // CB end point
}
